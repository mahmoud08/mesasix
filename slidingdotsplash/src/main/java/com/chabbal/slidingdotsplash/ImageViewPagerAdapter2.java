package com.chabbal.slidingdotsplash;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.ArrayRes;
import android.support.annotation.NonNull;
import android.support.annotation.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Johny on 11/01/2017.
 */

public class ImageViewPagerAdapter2 extends ViewPagerAdapter {

    private final int type; //2 for latest parteners
    private Context mContext;
    private OnItemClickListener mOnPagerItemClick;
    private OnSetImageListener mOnSetImageListener;
    private List<ItemModel> mImageResources;

    public ImageViewPagerAdapter2(Context context, @NonNull OnSetImageListener onSetImageListener, int type) {
        mContext = context;
        mOnSetImageListener = onSetImageListener;
        mImageResources = new ArrayList<>();
        this.type = type;
    }

    @Override
    public float getPageWidth(int position) {
        return 0.6f;
    }

    @Override
    public View getItem(final int position) {
        ImageView imageView = (ImageView) LayoutInflater.from(mContext).inflate(R.layout.item_view_pager_image, null);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnPagerItemClick != null) {
                    mOnPagerItemClick.onPagerItemClick(v, position);
                }
            }
        });
        if (mOnSetImageListener != null) {
            mOnSetImageListener.setImage(imageView, position);
        } else {
            imageView.setImageResource(mImageResources.get(position).getImageResource());
        }
        return imageView;
    }

    @Override
    public int getCount() {
        return mImageResources.size();
    }

    public void setImageResources(@SuppressLint("SupportAnnotationUsage") @NonNull @ArrayRes @Size(min = 2) List<ItemModel> imageResources) {
        mImageResources = imageResources;
        notifyDataSetChanged();
    }

    public void setOnPagerItemClick(@NonNull OnItemClickListener onPagerItemClickListener) {
        mOnPagerItemClick = onPagerItemClickListener;
    }
}
