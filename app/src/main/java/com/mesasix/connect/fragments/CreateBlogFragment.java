package com.mesasix.connect.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mesasix.connect.R;
import com.mesasix.connect.activities.MainActivity;
import com.mesasix.connect.models.CropParam;
import com.mesasix.connect.views.CropImageView;
import com.mesasix.connect.views.CropIntent;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CreateBlogFragment extends XmlFragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = null;

        v = inflater.inflate(R.layout.create_blog_fragment, container, false);

        /*final RichEditTextFragment richEditTextFragment =
                (RichEditTextFragment) getChildFragmentManager().findFragmentById(R.id.riched_fragmnt);*/
        //richEditTextFragment.icarus.setContent("<ul><li><b><i><u>Bcvbxbccb vc</u></i></b></li><li><b><i><u>Fxhnncbb</u></i></b></li></ul>");

        v.findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSave(view);
                /*richEditTextFragment.icarus.getContent(new Callback() {
                    @Override
                    public void run(String params) {
                        Log.d("content", params);
                    }
                });*/
            }
        });

        return v;
    }

    private void onClickSave(View view){}

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}