package com.mesasix.connect.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mesasix.connect.R;
import com.mesasix.connect.adapters.GeneralAdapter;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.TestModel;

import java.util.ArrayList;
import java.util.List;

public class PartenersFragment extends android.support.v4.app.Fragment {

    RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View view = inflater.inflate(R.layout.home_layout, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.news_rv);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.bringToFront();

        // specify an adapter (see also next example)
        List<TestModel> l = new ArrayList<>();
        TestModel mo;
        mo = new TestModel();
        mo.setViewType(1);
        mo.setName("ASDad");
        l.add(mo);
        for (int i = 16; i < 23; i++) {
            mo = new TestModel();
            mo.setViewType(i);
            mo.setName("ASDad");
            l.add(mo);
        }

        GeneralAdapter mAdapter = new GeneralAdapter(l, getActivity() , Constants.PARTENER_SCREEN);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

}