package com.mesasix.connect.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.mesasix.connect.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CreateFreelancerFragment extends Fragment {

    private static View view;
    String[] skillsNames = {
            "Actor",
            "Graphic Designer",
            "Production Interpreter/Translator",
            "Animator",
            "App Developer",
            "Grip",
            "Production Manager",
            "Hairdresser",
            "Aerial Filming",
            "Journalist",
            "Production Sound Assistant",
            "Lighting Assistant",
            "Art Director",
            "Production Sound Engineer",
            "Assistant / Associate Producer",
            "Lighting Operator",
            "Production Transport Assistant",
            "Assistant Director",
            "Line Producer",
            "Broadcast Engineer",
            "Location Assistant",
            "Production Transport Manager",
            "Broadcast Graphics Designer",
            "Make-up Artist",
            "Media Consultant",
            "Camera Assistant",
            "Media Management Operator",
            "Re-recording Mixer / Dubbing Mixer",
            "Composer",
            "Photographer",
            "Researcher",
            "Concept Designer / Artist",
            "Post Production Sound Editor",
            "Screenwriter / Scriptwriter",
            "Copywriter",
            "Costume Designer",
            "Post Production Supervisor",
            "Set Designer",
            "Creative Director",
            "Set Rigger",
            "Director",
            "PR & Communication / Marketing Consultant",
            "Social Media Specialist",
            "Special Effects Technician",
            "Event Manager",
            "Presenter",
            "Steadicam Operator",
            "Executive Producer",
            "Technical Director",
            "Floor Manager",
            "Production Accountant",
            "Transmissions Operator",
            "Gaffer",
            "Production Assistant",
            "TV Production Stylist",
            "Gallery Operator",
            "Production Coordinator",
            "Video Editor",
            "Production Designer",
            "Vision Mixer",
            "Production Engineer",
            "Voice Artist",
            "Wardrobe Stylist",
            "Web Developer"
    };

    List<View> childList = new ArrayList<>();
    View child = null;
    private boolean isAdded;
    private View currentView;
    private Context mContext;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {

        /*FragmentManager fm = getFragmentManager();

        Fragment xmlFragment = fm.findFragmentById(R.id.tessss);
        if (xmlFragment != null) {
            fm.beginTransaction().remove(xmlFragment).commit();
        }*/

        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.create_freelancer_fragment, container, false);
        /*} catch (InflateException e) {
        }*/

        /*final RichEditTextFragment richEditTextFragment =
                (RichEditTextFragment) getChildFragmentManager().findFragmentById(R.id.riched_fragmnt);
        *///richEditTextFragment.icarus.setContent("<ul><li><b><i><u>Bcvbxbccb vc</u></i></b></li><li><b><i><u>Fxhnncbb</u></i></b></li></ul>");
        TableLayout featuresTable = (TableLayout) view.findViewById(R.id.table_layout);
        Resources r = getActivity().getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, r.getDisplayMetrics());

        /*for (int i = 0; i < skillsNames.length; i++) {
            CheckBox feature1 = new CheckBox(getActivity());
            feature1.setId(i);
            feature1.setText(skillsNames[i]);
            feature1.setTag(skillsNames[i]);
            feature1.setTextColor(Color.parseColor("#575757"));
            feature1.setTextSize(15);
            feature1.setPadding(20, 0, 0, 0);
            featuresTable.addView(feature1);
        }


        /*featuresTable.setStretchAllColumns(false);
        TableLayout.LayoutParams params =
                new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(px,px,px,px);
        featuresTable.setLayoutParams(params);

        featuresTable.setWeightSum(2);

        for (int i = 0; i < skillsNames.length - 1; i++) {
            TableRow tableRow = new TableRow(getActivity());
            tableRow.setGravity(Gravity.CENTER);
            tableRow.setLayoutParams(
                    new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f));

            for (int j = 0; j < 3; j++) {
                CheckBox button = new CheckBox(getActivity());
                final int buttonNumber = (j + i);
                button.setText("asdadasdadasdadasdadad" + buttonNumber);
                button.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT));

                tableRow.addView(button);
            }
            featuresTable.addView(tableRow);
        }*/

        GridView gridview = (GridView) view.findViewById(R.id.cf_gridview);
        gridview.setAdapter(new ImageAdapter(getActivity(), skillsNames));

        return view;
    }

    class ImageAdapter extends BaseAdapter {
        private Context mContext;
        String names[];

        public ImageAdapter(Context c , String names[]) {
            mContext = c;
            this.names = names;
        }

        public int getCount() {
            return names.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            View v;
            TextView title = null;
            CheckBox checkBox = null;
            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                v = LayoutInflater.from(getContext()).inflate(R.layout.checkbox_list_item , null);
            } else {
                v = convertView;
            }
            title = v.findViewById(R.id.checkbox_title);
            checkBox = v.findViewById(R.id.checkbox);
            title.setText(names[position]);
            return v;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //mContext = null;
    }
}