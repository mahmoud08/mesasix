package com.mesasix.connect.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mesasix.connect.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CreatePartnerFragment extends Fragment {

    String[] categoriesNames = {
            "A/V Production & Services",
            "Advertising & Marketing",
            "Animation & Special FX",
            "Business Support Services",
            "Entertainment Devices",
            "Events",
            "Film",
            "Gaming",
            "Information & Data Services",
            "Mobile Content",
            "Music",
            "Print Publishing",
            "Public Relations",
            "Radio Broadcasting",
            "Rights Ownership",
            "Training",
            "TV Broadcasting",
            "Web Publishing & Services"
    };

    String[] mimeTypes =
            {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                    "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                    "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                    "text/plain",
                    "application/pdf",
                    "application/vnd.ms-powerpoint"};

    LinearLayout filesContainer;
    private static View view;

    List<View> childList = new ArrayList<>();
    View child = null;
    private boolean isAdded;
    private View currentView;
    private Context mContext;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {

        /*FragmentManager fm = getFragmentManager();

        Fragment xmlFragment = fm.findFragmentById(R.id.tessss);
        if (xmlFragment != null) {
            fm.beginTransaction().remove(xmlFragment).commit();
        }*/

        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
/*
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {*/
          View  view = inflater.inflate(R.layout.create_partner_fragment, container, false);
        /*} catch (InflateException e) {
        }*/

        /*final RichEditTextFragment richEditTextFragment =
                (RichEditTextFragment) getChildFragmentManager().findFragmentById(R.id.riched_fragmnt);
        *///richEditTextFragment.icarus.setContent("<ul><li><b><i><u>Bcvbxbccb vc</u></i></b></li><li><b><i><u>Fxhnncbb</u></i></b></li></ul>");
        LinearLayout featuresTable = (LinearLayout) view.findViewById(R.id.checkboxs_container2);
        for (int i = 0; i < 18; i++) {
            CheckBox feature1 = new CheckBox(getActivity());
            feature1.setId(i);
            feature1.setText(categoriesNames[i]);
            feature1.setTag(categoriesNames[i]);
            feature1.setTextColor(Color.parseColor("#575757"));
            feature1.setTextSize(15);
            feature1.setPadding(20, 0, 0, 0);
            featuresTable.addView(feature1);
        }

        filesContainer = view.findViewById(R.id.files_container);
        //filesContainer.removeViewAt();

        (view.findViewById(R.id.add_file)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAdded = true;
                pickFile();
            }
        });

        return view;
    }

    private void pickFile() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }

        startActivityForResult(intent, 1212);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1212:
                if (resultCode == Activity.RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    File myFile = new File(uriString);
                    String path = myFile.getAbsolutePath();

                    String displayName = null;

                    if (isAdded) {
                        child = LayoutInflater.from(getActivity()).inflate(R.layout.file_list_item, null);
                        child.setId(childList.indexOf(child));
                        childList.add(child);
                        filesContainer.addView(child, childList.indexOf(child));
                    } else {
                        child = currentView;
                    }

                    child.findViewById(R.id.delete_file).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            filesContainer.removeViewAt(childList.indexOf(((View) view.getParent().getParent())));
                            childList.remove(((View) view.getParent().getParent()));
                        }
                    });

                    child.findViewById(R.id.choose_file_layout).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isAdded = false;
                            currentView = ((View) view.getParent().getParent());
                            pickFile();
                        }
                    });

                    child.findViewById(R.id.choose_file_btn).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            isAdded = false;
                            currentView = ((View) view.getParent().getParent());
                            pickFile();
                        }
                    });

                    if (uriString.startsWith("content://")) {
                        Cursor cursor = null;
                        try {
                            cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                            }
                        } finally {
                            cursor.close();
                        }
                    } else if (uriString.startsWith("file://")) {
                        displayName = myFile.getName();
                    }

                    if (!displayName.isEmpty())
                        ((TextView) child.findViewById(R.id.file_name)).setText(displayName);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //mContext = null;
    }
}