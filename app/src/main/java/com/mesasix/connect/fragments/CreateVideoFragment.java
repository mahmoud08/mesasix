package com.mesasix.connect.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mesasix.connect.R;
import com.mesasix.connect.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class CreateVideoFragment extends XmlFragment {

    List<View> childList = new ArrayList<>();
    View child = null;
    private boolean isAdded;
    private View currentView;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = null;

        v = inflater.inflate(R.layout.create_video_fragment, container, false);


        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}