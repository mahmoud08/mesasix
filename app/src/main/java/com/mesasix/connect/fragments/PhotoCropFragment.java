package com.mesasix.connect.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mesasix.connect.R;
import com.mesasix.connect.models.CropParam;
import com.mesasix.connect.views.CropImageView;
import com.mesasix.connect.views.CropIntent;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class PhotoCropFragment extends Fragment {

    public static final String CROPPED_IMAGE_FILEPATH = "/sdcard/cropped.jpg";

    private Bitmap mBitmap;
    private Uri mInputPath = null;
    private Uri mOutputPath = null;
    private CropImageView mCropImageView;

    protected static void closeSilently(Closeable c) {
        if (c == null) return;
        try {
            c.close();
        } catch (Throwable t) {
        }
    }

    public static CropParam getCropParam(Intent intent) {
        CropParam params = new CropParam();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey(CropIntent.ASPECT_X) && extras.containsKey(CropIntent.ASPECT_Y)) {
                params.mAspectX = extras.getInt(CropIntent.ASPECT_X);
                params.mAspectY = extras.getInt(CropIntent.ASPECT_Y);
            }
            if (extras.containsKey(CropIntent.OUTPUT_X) && extras.containsKey(CropIntent.OUTPUT_Y)) {
                params.mOutputX = extras.getInt(CropIntent.OUTPUT_X);
                params.mOutputY = extras.getInt(CropIntent.OUTPUT_Y);
            }
            if (extras.containsKey(CropIntent.MAX_OUTPUT_X) && extras.containsKey(CropIntent.MAX_OUTPUT_Y)) {
                params.mMaxOutputX = extras.getInt(CropIntent.MAX_OUTPUT_X);
                params.mMaxOutputY = extras.getInt(CropIntent.MAX_OUTPUT_Y);
            }
        }
        return params;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Intent intent = getActivity().getIntent();
        Bundle extras = intent.getExtras();

        /*if (extras == null) {
            //getActivity().setResult(Activity.RESULT_CANCELED);
            return;
        }
*/
        if (mOutputPath == null) {
            String defaultPath = getActivity().getCacheDir().getPath() + "tmp.jpg";
            mOutputPath = Uri.fromFile(new File(defaultPath));
        }

        mInputPath = intent.getData();
        if (mInputPath == null) {
            //startPickImage();
            return;
        }

        mBitmap = loadBitmap(mInputPath);
        if (mBitmap == null) {
            getActivity().setResult(Activity.RESULT_CANCELED);
            //finish();
            return;
        }

        mCropImageView.initialize(mBitmap, getCropParam(intent));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = null;
        v = inflater.inflate(R.layout.photo_crop_fragment, container, false);
        mCropImageView = (CropImageView) v.findViewById(R.id.CropWindow);

        v.findViewById(R.id.pick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPickImage();
            }
        });

        return v;
    }

    @Override
    public void onDestroy() {
        mBitmap = null;
        mCropImageView.destroy();
        super.onDestroy();
    }

    private void startCropImage() {

        // Create a CropIntent
        CropIntent intent = new CropIntent();

        // Set the source image filepath/URL and output filepath/URL (Optional)
        //intent.setImagePath("/sdcard/source.jpg");
        intent.setOutputPath(CROPPED_IMAGE_FILEPATH);

        // Set a fixed crop window size (Optional)
        //intent.setOutputSize(640,480);

        // set the max crop window size (Optional)
        //intent.setMaxOutputSize(800,600);

        // Set a fixed crop window's width/height aspect (Optional)
        //intent.setAspect(3,2);

        // start ImageCropper activity with certain request code and listen for result
        startActivityForResult(intent.getIntent(getActivity()), 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == 0) {
            mInputPath = data.getData();
            mBitmap = loadBitmap(mInputPath);
            if (mBitmap == null) {
                getActivity().setResult(Activity.RESULT_CANCELED);
                //getActivity().finish();
                return;
            }
            mCropImageView.initialize(mBitmap, getCropParam(getActivity().getIntent()));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onClickSave(View v) {
        mCropImageView.crop();
        new SaveImageTask().execute(mCropImageView.getCropBitmap());
    }

    public Bitmap getCroppedImage() {
        mCropImageView.crop();
        new SaveImageTask().execute(mCropImageView.getCropBitmap());
        return mCropImageView.getCropBitmap();
    }

    protected Bitmap loadBitmap(Uri uri) {

        Bitmap bitmap = null;
        try {
            InputStream in = getActivity().getContentResolver().openInputStream(uri);
            bitmap = BitmapFactory.decodeStream(in);
            in.close();
        } catch (FileNotFoundException e) {
            Toast.makeText(getActivity(), "Can't found image file !", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(getActivity(), "Can't load source image !", Toast.LENGTH_LONG).show();
        }
        return bitmap;
    }

    protected Bitmap loadBitmapWithInSample(Uri uri) {

        final int MAX_VIEW_SIZE = 1024;

        InputStream in = null;
        try {
            in = getActivity().getContentResolver().openInputStream(uri);
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();

            int scale = 1;
            if (o.outHeight > MAX_VIEW_SIZE || o.outWidth > MAX_VIEW_SIZE) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(MAX_VIEW_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            in = getActivity().getContentResolver().openInputStream(uri);
            Bitmap b = BitmapFactory.decodeStream(in, null, o2);
            in.close();

            return b;
        } catch (FileNotFoundException e) {

        } catch (IOException e) {

        }
        return null;
    }

    protected void startPickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, 0);
    }

    private class SaveImageTask extends AsyncTask<Bitmap, Void, Boolean> {

        private ProgressDialog mProgressDailog;

        private SaveImageTask() {
            mProgressDailog = new ProgressDialog(getActivity());
            mProgressDailog.setCanceledOnTouchOutside(false);
            mProgressDailog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            mProgressDailog.setTitle("Save");
            mProgressDailog.setMessage("Save");
            // mProgressDailog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            /*if (mProgressDailog.isShowing()) {
                mProgressDailog.dismiss();
            }
            getActivity().setResult(Activity.RESULT_OK, new Intent().putExtra(MediaStore.EXTRA_OUTPUT, mOutputPath));*/
            //finish();
        }

        @Override
        protected Boolean doInBackground(Bitmap... params) {
            OutputStream outputStream = null;
            try {
                outputStream = getActivity().getContentResolver().openOutputStream(mOutputPath);
                if (outputStream != null) {
                    params[0].compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
                }
            } catch (IOException e) {

            } finally {
                closeSilently(outputStream);
            }

            return Boolean.TRUE;
        }
    }
}