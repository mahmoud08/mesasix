package com.mesasix.connect.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mesasix.connect.R;
import com.mesasix.connect.activities.MainActivity;
import com.mesasix.connect.adapters.HomeAdapter;
import com.mesasix.connect.models.TestModel;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends android.support.v4.app.Fragment {

    RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        getActivity().findViewById(R.id.fab).setVisibility(View.VISIBLE);
        ((TextView)getActivity().findViewById(R.id.menu_board_txt)).setText("partners' community");
        ((MainActivity)getActivity()).enableDisableDrawer(true);
        getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
        View view = inflater.inflate(R.layout.home_layout, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.news_rv);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.bringToFront();

        // specify an adapter (see also next example)
        List<TestModel> l = new ArrayList<>();
        TestModel mo;
        for (int i = 1; i < 23; i++) {
            mo = new TestModel();
            mo.setViewType(i);
            mo.setName("ASDad");
            l.add(mo);
        }

        HomeAdapter mAdapter = new HomeAdapter(l, getActivity());
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}