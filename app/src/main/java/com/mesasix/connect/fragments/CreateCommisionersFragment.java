package com.mesasix.connect.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mesasix.connect.R;
import com.mesasix.connect.activities.MainActivity;

public class CreateCommisionersFragment extends XmlFragment {

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        final View v = inflater.inflate(R.layout.create_commissioner_fragment, container, false);

        FragmentManager manager = ((Activity)getActivity()).getFragmentManager();

        final PhotoCropFragment fff =
                (PhotoCropFragment) manager.findFragmentById(R.id.crop_img_frag);

        v.findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSave(view);
                Bitmap bm = fff.getCroppedImage();
                ((ImageView)v.findViewById(R.id.test_imgview)).setImageBitmap(bm);
                /*richEditTextFragment.icarus.getContent(new Callback() {
                    @Override
                    public void run(String params) {
                        Log.d("content", params);
                    }
                });*/
            }
        });



        return v;
    }

    public void onClickSave(View v) {
    }
}