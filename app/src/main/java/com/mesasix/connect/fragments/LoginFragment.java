package com.mesasix.connect.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mesasix.connect.R;
import com.mesasix.connect.activities.MainActivity;
import com.mesasix.connect.views.HoveredTextView;
import com.squareup.picasso.Picasso;

public class LoginFragment extends android.support.v4.app.Fragment {

    EditText userMail, passwrod , regUserMail;
    RelativeLayout login , register;
    CheckBox rememberMe;
    TextView forgetPass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        getActivity().findViewById(R.id.fab).setVisibility(View.GONE);
        ((TextView)getActivity().findViewById(R.id.menu_board_txt)).setText("get on board!");
        ((MainActivity)getActivity()).enableDisableDrawer(false);
        getActivity().findViewById(R.id.toolbar).setVisibility(View.GONE);
        final View v = inflater.inflate(R.layout.login_fragment, container, false);
        login = v.findViewById(R.id.login_btn);
        register = v.findViewById(R.id.reg_btn);
        userMail = (EditText) v.findViewById(R.id.user_email);
        regUserMail = (EditText) v.findViewById(R.id.reg_user_email);
        passwrod = (EditText) v.findViewById(R.id.user_pass);
        forgetPass = (HoveredTextView)v.findViewById(R.id.forget_pass);
        rememberMe = (CheckBox)v.findViewById(R.id.remember_me);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (userName.getText().toString().isEmpty()) {
                    userName.setError("Please enter your email address");
                    return;
                }
                if (passwrod.getText().toString().isEmpty()) {
                    passwrod.setError("Please enter your password");
                    return;
                }
*/
                HomeFragment homeFragment = new HomeFragment();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, homeFragment);
                //ft.addToBackStack("Home_Frag");
                getActivity().getSupportFragmentManager().beginTransaction().remove(LoginFragment.this);
                ft.addToBackStack(null);
                ft.commit();


              // scrollToView(((NestedScrollView)v.findViewById(R.id.nested_scroll_view)) , v.findViewById(R.id.reg_parent));

            }
        });

       // Picasso.get().load("https://img.youtube.com/vi/AE9oLxhSXig/hqdefault.jpg").into(image);

        return v;
    }

    /**
     * Used to scroll to the given view.
     *
     * @param scrollViewParent Parent ScrollView
     * @param view View to which we need to scroll.
     */
    private void scrollToView(final NestedScrollView scrollViewParent, final View view) {
        // Get deepChild Offset
        Point childOffset = new Point();
        getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset);
        // Scroll to child.
        scrollViewParent.smoothScrollTo(0, childOffset.y);
    }

    /**
     * Used to get deep child offset.
     * <p/>
     * 1. We need to scroll to child in scrollview, but the child may not the direct child to scrollview.
     * 2. So to get correct child position to scroll, we need to iterate through all of its parent views till the main parent.
     *
     * @param mainParent        Main Top parent.
     * @param parent            Parent.
     * @param child             Child.
     * @param accumulatedOffset Accumulated Offset.
     */
    private void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final
    Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }


}