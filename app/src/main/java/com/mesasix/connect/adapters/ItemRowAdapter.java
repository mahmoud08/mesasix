package com.mesasix.connect.adapters;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.chabbal.slidingdotsplash.OnItemClickListener;
import com.mesasix.connect.R;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.LatestNewsModel;

import java.util.List;

public class ItemRowAdapter extends RecyclerView.Adapter<ItemRowAdapter.ViewHolder> implements OnItemClickListener {
    private final Context context;
    private LayoutInflater lInflater;
    private List<LatestNewsModel> listStorage;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ItemRowAdapter(Context context, List<LatestNewsModel> items) {
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = items;
        this.context = context;
    }



    // Create new views (invoked by the layout manager)
    @Override
    public ItemRowAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        View v;
        if (viewType == 1)
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_row_first, parent, false);
        else
            v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_row, parent, false);
        return new ViewHolder(v, viewType);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return 1;
        else
            return super.getItemViewType(position);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ItemRowAdapter.ViewHolder holder, int position) {
        /*holder.title.setSpanSize(25,
                25, 1);
        holder.title.setText(Constants.setSpan(context, "e-services for individuals", 25, 25));
        holder.title.setTypeface(Constants.setTitleTypeFace(context));*/
        if(position == 0){
            holder.title.setTypeface(Constants.setTitleTypeFace(context));
            holder.date.setTypeface(Constants.setTitleTypeFace(context));
            holder.posted.setTypeface(Constants.setTitleTypeFace(context));
            holder.cat.setTypeface(Constants.setTitleTypeFace(context));
        }

        if (position > 0) {
            holder.viewDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Rect r = Constants.locateView(view);
                    LayoutInflater lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View popup_view = lInflater.inflate(R.layout.popup_layout, null);
                    final PopupWindow popup = new PopupWindow(popup_view, FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, true);
                    popup.setFocusable(true);
                    popup.setBackgroundDrawable(new ColorDrawable());
                    popup.showAtLocation(view, Gravity.TOP | Gravity.CENTER, r.centerX() - 715, r.bottom + 10);
                }
            });
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return listStorage.size();
    }

    @Override
    public void onPagerItemClick(View view, int position) {
        //on freelancer item list clicked
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView posted, cat;
        TextView title;
        TextView date;
        TextView readMore;
        ImageView viewDelete;

        //This constructor would switch what to findViewBy according to the type of viewType
        public ViewHolder(View v, int viewType) {
            super(v);
            title = (TextView) v.findViewById(R.id.row_title);
            date = (TextView) v.findViewById(R.id.date);
            posted = (TextView) v.findViewById(R.id.posted_by);
            cat = (TextView) v.findViewById(R.id.category);
            viewDelete = v.findViewById(R.id.row_edit_delete_btn);
        }
    }
}