package com.mesasix.connect.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mesasix.connect.R;
import com.mesasix.connect.fragments.TermsFragment;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.LatestNewsModel;
import com.mesasix.connect.models.TestModel;
import com.mesasix.connect.views.CenteredImageSpan;
import com.mesasix.connect.views.HoveredTextViewWithSpan;

import java.util.List;

public class BriefsAdapter extends ArrayAdapter<LatestNewsModel> {

    private final Context context;
    private LayoutInflater lInflater;
    private List<LatestNewsModel> listStorage;

    public BriefsAdapter(Context context, int resource, List<LatestNewsModel> items) {
        super(context, resource, items);
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public LatestNewsModel getItem(int position) {
        return listStorage.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateList(List<TestModel> list) {

    }

    private void setSpan(ViewHolder holder) {
        SpannableString ssBuilder = new SpannableString("RFP: Media and PR Agency - Department of Economic Development  ");
        Drawable newIcon = context.getResources().getDrawable(R.mipmap.if_new_36223);
        ((HoveredTextViewWithSpan) holder.title).setSpanSize(60, 60, 2);
        newIcon.setBounds(0, 0, 60, 60);
        CenteredImageSpan newIconSpan = new CenteredImageSpan(newIcon, ImageSpan.ALIGN_BASELINE);
        newIconSpan.setyPos(-8);


        ssBuilder.setSpan(
                newIconSpan, // Span to add
                ssBuilder.length() - 1, // Start of the span (inclusive)
                ssBuilder.length(), // End of the span (exclusive)
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);// Do not extend the span when text add later
        holder.title.setText(ssBuilder);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = lInflater.inflate(R.layout.brief_list_item, parent, false);
            holder.title = (HoveredTextViewWithSpan) convertView.findViewById(R.id.brief_item_title);
            /*holder.date = (TextView) convertView.findViewById(R.id.latest_news_date);*/
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //holder.title.setTypeface(Constants.setTitleTypeFace(context));
        holder.title.setSpanSize(35, 12, 2);
        SpannableString title = new SpannableString("RFP: Media and PR Agency - Department of Economic\nDeveloment   ");
        holder.title.setText(Constants.setSpan(context, title, (int) Constants.dpFromPx(35, context)
                , (int) Constants.dpFromPx(12, context), R.mipmap.if_new_36223, -30));

        convertView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getAction();
                if (action == MotionEvent.ACTION_DOWN
                        || action == MotionEvent.ACTION_HOVER_ENTER) {
                    view.findViewById(R.id.hover_readmore_txt).setVisibility(View.VISIBLE);
                    view.setBackgroundColor(Color.parseColor("#EFEFEF"));
                    FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                    TermsFragment fragment = new TermsFragment(Constants.BRIEF_SINGLE_ITEM_SCREEN);
                    ft.replace(R.id.fragment_container, fragment);
                    //clear login shared pref
                    ft.addToBackStack(null);
                    ft.commit();

                } /*else if (action == MotionEvent.ACTION_MOVE) {
                    view.findViewById(R.id.hover_readmore_txt).setVisibility(View.VISIBLE);
                    view.setBackgroundColor(Color.parseColor("#EFEFEF"));
                }*/ else if (action == MotionEvent.ACTION_UP
                        || action == MotionEvent.ACTION_CANCEL
                        || action == MotionEvent.ACTION_HOVER_EXIT
                        || action == MotionEvent.ACTION_OUTSIDE) {
                    view.findViewById(R.id.hover_readmore_txt).setVisibility(View.GONE);
                    view.setBackgroundColor(Color.TRANSPARENT);
                }
                return true;
            }
        });
        /*holder.title.setText(listStorage.get(position).getTitle());
        holder.date.setText(listStorage.get(position).getDate());*/
        return convertView;
    }

    static class ViewHolder {
        HoveredTextViewWithSpan title;
        TextView date;
        TextView readMore;
    }
}