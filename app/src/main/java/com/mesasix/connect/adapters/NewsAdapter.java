package com.mesasix.connect.adapters;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chabbal.slidingdotsplash.OnItemClickListener;
import com.mesasix.connect.R;
import com.mesasix.connect.fragments.TermsFragment;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.LatestNewsModel;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> implements OnItemClickListener {
    private final Context context;
    private LayoutInflater lInflater;
    private List<LatestNewsModel> listStorage;

    // Provide a suitable constructor (depends on the kind of dataset)
    public NewsAdapter(Context context, List<LatestNewsModel> items) {
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = items;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_events_list_item, parent, false);
        return new ViewHolder(v, viewType);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(NewsAdapter.ViewHolder holder, int position) {
        /*holder.title.setSpanSize(25,
                25, 1);
        holder.title.setText(Constants.setSpan(context, "e-services for individuals", 25, 25));
        holder.title.setTypeface(Constants.setTitleTypeFace(context));*/
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                TermsFragment fragment = new TermsFragment(Constants.NEWS_SINGLE_ITEM_SCREEN);
                ft.replace(R.id.fragment_container, fragment);
                //clear login shared pref
                ft.addToBackStack(null);
                ft.commit();
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return listStorage.size();
    }

    @Override
    public void onPagerItemClick(View view, int position) {
        //on freelancer item list clicked
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView date;
        TextView readMore;

        //This constructor would switch what to findViewBy according to the type of viewType
        public ViewHolder(View v, int viewType) {
            super(v);
            //itle = (AppCompatTextView) v.findViewById(R.id.par_dir_item_title);
        }
    }
}