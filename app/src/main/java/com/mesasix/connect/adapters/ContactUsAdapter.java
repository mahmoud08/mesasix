package com.mesasix.connect.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mesasix.connect.R;
import com.mesasix.connect.models.ContactUsModel;
import com.mesasix.connect.models.LatestNewsModel;
import com.mesasix.connect.models.TestModel;

import java.util.List;

public class ContactUsAdapter extends ArrayAdapter<ContactUsModel> {

    private final Context context;
    private LayoutInflater lInflater;
    private List<ContactUsModel> listStorage;

    public ContactUsAdapter(Context context, int resource, List<ContactUsModel> items) {
        super(context, resource, items);
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public ContactUsModel getItem(int position) {
        return listStorage.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateList(List<TestModel> list){

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = lInflater.inflate(R.layout.contact_us_list_item, parent, false);
            holder.title = (TextView) convertView.findViewById(R.id.contact_us_title);
            holder.brief = (TextView) convertView.findViewById(R.id.contact_us_brief);
            holder.mobile = (TextView) convertView.findViewById(R.id.contact_us_mobile);
            holder.mail = (TextView) convertView.findViewById(R.id.contact_us_mail);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(listStorage.get(position).getName());
        holder.brief.setText(listStorage.get(position).getDesc());
        holder.mobile.setText(listStorage.get(position).getMobile());
        holder.mail.setText(listStorage.get(position).getMail());

        return convertView;
    }

    static class ViewHolder {
        TextView title;
        TextView brief;
        TextView mobile;
        TextView mail;

    }
}