package com.mesasix.connect.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.chabbal.slidingdotsplash.ItemModel;
import com.chabbal.slidingdotsplash.OnItemClickListener;
import com.chabbal.slidingdotsplash.OnSetImageListener;
import com.chabbal.slidingdotsplash.SlidingSplashView;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mesasix.connect.R;
import com.mesasix.connect.activities.MainActivity;
import com.mesasix.connect.fragments.AboutFragment;
import com.mesasix.connect.fragments.ContactUsFragment;
import com.mesasix.connect.fragments.LoginFragment;
import com.mesasix.connect.fragments.TermsFragment;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.IndustryNewsModel;
import com.mesasix.connect.models.LatestNewsModel;
import com.mesasix.connect.models.TestModel;
import com.mesasix.connect.views.CenteredImageSpan;
import com.mesasix.connect.views.HoveredTextView;
import com.mesasix.connect.views.HoveredTextViewWithSpan;
import com.mesasix.connect.views.NonSwipeableViewPager;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> implements OnItemClickListener {
    private List<TestModel> mDataset;
    private Context context;
    private OnSetImageListener mOnSetImageListener;

    // Provide a suitable constructor (depends on the kind of dataset)
    public HomeAdapter(List<TestModel> myDataset, Context context) {
        mDataset = myDataset;
        this.context = context;
    }

    public static float dpFromPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public void setOnShowImageListener(OnSetImageListener onShowImageListener) {
        mOnSetImageListener = onShowImageListener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v;
        switch (viewType) {

            case 1:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.news_item_view_1, parent, false);
                return new ViewHolder(v, viewType);
            case 2:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.news_item_view_2, parent, false);
                return new ViewHolder(v, viewType);
            case 4:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.brief_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 3:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.inbox_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 5:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.latest_news_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 6:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.latest_events_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 7:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.industry_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 8:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.eservice_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 9:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.discover_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 10:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.offers_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 11:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.orders_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 12:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.ney_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 13:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.help_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 14:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.freelancer_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 15:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.latest_partener_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 16:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.mesasix_admin_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 17:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.add_news_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 18:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.classified_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 19:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.twofour_services_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 20:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.feedback_request_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 21:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.job_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 22:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.home_footer_view, parent, false);
                return new ViewHolder(v, viewType);
            default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.news_item_view_1, parent, false);
                return new ViewHolder(v, viewType);
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final HomeAdapter.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
            case 2:
            case 3:
            case 4:
                SpannableString ssBuilder;
                Drawable doubleArrowIcon = context.getResources().getDrawable(R.drawable.arrows_unselected);
                int width = (int) dpFromPx(holder.getItemViewType() == 1 ? 35 : 30, context);
                int height = (int) dpFromPx(holder.getItemViewType() == 1 ? 35 : 30, context);
                ((HoveredTextViewWithSpan) holder.title).setSpanSize(holder.getItemViewType() == 1 ? 35 : 30,
                        holder.getItemViewType() == 1 ? 35 : 30, 1);
                doubleArrowIcon.setBounds(0, 0, width, height);
                CenteredImageSpan doubleArrow = new CenteredImageSpan(doubleArrowIcon, ImageSpan.ALIGN_BASELINE);
                if (holder.getItemViewType() == 1)
                    ssBuilder = new SpannableString("Connect Newsletter - Feb 2018 ");
                else if (holder.getItemViewType() == 2)
                    ssBuilder = new SpannableString("UAE Exchange Provides Payment Solutions ");
                else if (holder.getItemViewType() == 3) {
                    ssBuilder = new SpannableString("inbox ");
                    width = (int) dpFromPx(25, context);
                    height = (int) dpFromPx(10, context);

                    SpannableString inboxTitleOne = new SpannableString("New brief: RFQ#32/twofour54/2018 - Connect Events  ");
                    SpannableString inboxTitleTwo = new SpannableString("New brief: Media Buying Services Agency  ");

                    ((HoveredTextViewWithSpan) holder.title).setSpanSize(25, 25, 1);

                    ((HoveredTextViewWithSpan) holder.inboxTitleOne).setSpanSize(25,
                            10, 2);
                    ((HoveredTextViewWithSpan) holder.inboxTitleTwo).setSpanSize(25,
                            10, 2);

                    holder.inboxTitleOne.setText(Constants.setSpan(context, inboxTitleOne, width
                            , height, R.mipmap.if_new_36223, -25));
                    holder.inboxTitleTwo.setText(Constants.setSpan(context, inboxTitleTwo, width
                            , height, R.mipmap.if_new_36223, -25));

                    SpannableString inboxCount = new SpannableString("25 unread messages  ");
                    ((HoveredTextViewWithSpan) holder.inboxCount).setSpanSize(15
                            , 15, 2);
                    holder.inboxCount.setText(Constants.setSpan(context, inboxCount, (int) dpFromPx(15, context)
                            , (int) dpFromPx(15, context), R.drawable.span_arrow, -2));
                    width = (int) dpFromPx(25, context);
                    height = (int) dpFromPx(25, context);
                } else {
                    ssBuilder = new SpannableString("briefing room ");
                    width = (int) dpFromPx(25, context);
                    height = (int) dpFromPx(10, context);
                    SpannableString briefTitleOne = new SpannableString("RFQ#32/twofour54/2018 - Connect\nEvents  ");

                    ((HoveredTextViewWithSpan) holder.title).setSpanSize(25, 25, 1);

                    ((HoveredTextViewWithSpan) holder.briefTitleOne).setSpanSize(25,
                            10, 2);
                    holder.briefTitleOne.setText(Constants.setSpan(context, briefTitleOne, width
                            , height, R.mipmap.if_new_36223, -25));

                    SpannableString inboxCount = new SpannableString("1 new brief ");

                    ((HoveredTextViewWithSpan) holder.briefCount).setSpanSize(15
                            , 15, 2);
                    holder.briefCount.setText(Constants.setSpan(context, inboxCount, (int) dpFromPx(15, context)
                            , (int) dpFromPx(15, context), R.drawable.span_arrow, -2));

                    width = (int) dpFromPx(25, context);
                    height = (int) dpFromPx(25, context);
                }
                ssBuilder.setSpan(
                        doubleArrow, // Span to add
                        ssBuilder.length() - 1, // Start of the span (inclusive)
                        ssBuilder.length(), // End of the span (exclusive)
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);// Do not extend the span when text add later
                //holder.title.setText(ssBuilder);
                holder.title.setText(Constants.setSpan(context, ssBuilder, width,
                        height, R.drawable.arrows_unselected, 0));
                break;
            case 5:
            case 6:
                width = (int) dpFromPx(25, context);
                height = (int) dpFromPx(25, context);
                ((HoveredTextViewWithSpan) holder.title).setSpanSize(25, 25, 1);
                ssBuilder = new SpannableString(holder.getItemViewType() == 5 ? "latest news " : "latest events ");

                holder.title.setText(Constants.setSpan(context, ssBuilder, width,
                        height, R.drawable.arrows_unselected, 0));

                WizardPagerAdapter adapter = new WizardPagerAdapter(holder.v, holder.getItemViewType());
                holder.viewPager.setAdapter(adapter);
                holder.mTabLayout.setupWithViewPager(holder.viewPager);
                setTabStyle(holder.mTabLayout, holder.getItemViewType());
                holder.viewPager.setOffscreenPageLimit(4);
                try {
                    initLnewsList(holder);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 7:
                width = (int) dpFromPx(25, context);
                height = (int) dpFromPx(25, context);
                ((HoveredTextViewWithSpan) holder.title).setSpanSize(25, 25, 1);
                ssBuilder = new SpannableString("industry news ");
                holder.title.setText(Constants.setSpan(context, ssBuilder, width,
                        height, R.drawable.arrows_unselected, 0));
                try {
                    initIndustryNewsList(holder);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 8:
                width = (int) dpFromPx(25, context);
                height = (int) dpFromPx(25, context);
                ((HoveredTextViewWithSpan) holder.title).setSpanSize(25, 25, 1);
                ssBuilder = new SpannableString("e-services ");
                holder.title.setText(Constants.setSpan(context, ssBuilder, width,
                        height, R.drawable.arrows_unselected, 0));
                break;
            case 9:
                String text = "<font color='#9ACA3C'>twofour54° <br /> government <br /> services </font> helps <br /> businesses " +
                        "effortlessly set <br /> up and operate <br /> " +
                        "from the <br /> <font color='#9ACA3C'>twofour54°</font> <br /> campus.";
                holder.title.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
                width = (int) dpFromPx(25, context);
                height = (int) dpFromPx(25, context);
                ((HoveredTextViewWithSpan) holder.discovrAction).setSpanSize(25, 25, 3);
                ssBuilder = new SpannableString("discover ");
                holder.discovrAction.setText(Constants.setSpan(context, ssBuilder, width,
                        height, R.drawable.arrows_unselected, 0));
                break;
            case 10:
                width = (int) dpFromPx(25, context);
                height = (int) dpFromPx(25, context);
                ((HoveredTextViewWithSpan) holder.title).setSpanSize(25, 25, 1);
                ssBuilder = new SpannableString("latest offers ");
                holder.title.setText(Constants.setSpan(context, ssBuilder, width,
                        height, R.drawable.arrows_unselected, 0));
                break;
            case 11:
                width = (int) dpFromPx(25, context);
                height = (int) dpFromPx(25, context);
                ((HoveredTextViewWithSpan) holder.title).setSpanSize(25, 25, 1);
                ssBuilder = new SpannableString("order food ");
                holder.title.setText(Constants.setSpan(context, ssBuilder, width,
                        height, R.drawable.arrows_unselected, 0));
                holder.resturantsGV.setAdapter(new ResturantsAdapter(context, new String[]{"Zomato",
                        "Food on Click",
                        "Talabat",
                        "24h", "Allday Minimart"}));
                break;
            case 12:
                width = (int) dpFromPx(30, context);
                height = (int) dpFromPx(30, context);
                ((HoveredTextViewWithSpan) holder.title).setSpanSize(30, 30, 1);
                ssBuilder = new SpannableString("Breakfast at Ney ");
                holder.title.setText(Constants.setSpan(context, ssBuilder, width,
                        height, R.drawable.arrows_unselected, 0));
                break;
            case 14:
                List<ItemModel> list = new ArrayList<>();
                ItemModel model = new ItemModel();
                model.setName("Ahmad Omar");
                model.setBrief("Camera Assistant, Assistant / Associate Producer, Camera Operator, Lighting Operator, Grip");
                model.setImageResource(R.mipmap.freelancer);
                list.add(model);
                model = new ItemModel();
                model.setName("Patrick");
                model.setBrief("Director of Photography, Video Editor, Director");
                model.setImageResource(R.mipmap.freelancer);
                list.add(model);
                holder.splashView.initViewPager(context, list, 1, this);
                break;
            case 15:
                List<ItemModel> list2 = new ArrayList<>();
                ItemModel model2 = new ItemModel();
                model2.setImageResource(R.drawable.test);
                list2.add(model2);
                model2 = new ItemModel();
                model2.setImageResource(R.drawable.test);
                list2.add(model2);
                holder.splashView.initViewPager(context, list2, 2, this);
                break;
            case 16:
                holder.commissionersAdmin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragmentTransaction ft = ((AppCompatActivity) context)
                                .getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.COMMISSIONERS_LISTING_SCREEN));
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                });
                holder.partenerAdmin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TermsFragment loginFragment = new TermsFragment(Constants.PARTNER_LISTING_SCREEN);
                        FragmentTransaction ft = ((MainActivity)context).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, loginFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                });
                holder.userManagerAdmin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TermsFragment loginFragment = new TermsFragment(Constants.USER_MANAGEMENT_LISTING_SCREEN);
                        FragmentTransaction ft = ((MainActivity)context).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, loginFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                });
                break;
            case 20:
                holder.spinner.setItems("select" , "get in touch" , "service request", "feedback");
                break;
            case 22:
                holder.aboutBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TermsFragment loginFragment = new TermsFragment(Constants.ABOUT_SCREEN);
                        FragmentTransaction ft = ((MainActivity)context).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, loginFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                });
                holder.termsBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TermsFragment loginFragment = new TermsFragment(Constants.TERMS_SCREEN);
                        FragmentTransaction ft = ((MainActivity)context).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, loginFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                });
                holder.feedbackBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ContactUsFragment loginFragment = new ContactUsFragment();
                        FragmentTransaction ft = ((MainActivity)context).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, loginFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                });
                break;
        }
    }

    private void initIndustryNewsList(ViewHolder holder) throws Exception {
        List<IndustryNewsModel> list = new ArrayList<>();
        IndustryNewsModel m = new IndustryNewsModel();
        m.setDate("Mar 22, 2018");
        m.setTitle("TBWA bags eight Lynx Grands Prix");
        m.setFeature("Featured – Campaign Middle East");
        m.setDetails("It was TBWA’s year at the 2018 Dubai Lynx awards, held at Dubai’s Madinat Jumeirah on Wednesday, Mar ...");
        list.add(m);
        m = new IndustryNewsModel();
        m.setDate("Mar 14, 2018");
        m.setFeature("Featured – Campaign Middle East");
        m.setDetails("The Dubai Lynx Advertising Person of the Year is no stranger to awards, but BBDO boss Dani Richa tel ...");
        m.setTitle("Dani Richa: Dubai Lynx Advertising Person of the Year 2018");
        list.add(m);
        m = new IndustryNewsModel();
        m.setDate("Mar 14, 2018");
        m.setFeature("Featured – Campaign Middle East");
        m.setDetails("The Dubai Lynx Advertising Person of the Year is no stranger to awards, but BBDO boss Dani Richa tel ...");
        m.setTitle("Dani Richa: Dubai Lynx Advertising Person of the Year 2018");
        list.add(m);
        IndustryNewsAdapter newsAdapter = new IndustryNewsAdapter(context, R.layout.industry_news_list_item, list);
        holder.industryLv.setAdapter(newsAdapter);
    }

    private void initLnewsList(ViewHolder holder) throws Exception {
        List<LatestNewsModel> list = new ArrayList<>();
        List<LatestNewsModel> list2 = new ArrayList<>();
        List<LatestNewsModel> list3 = new ArrayList<>();
        List<LatestNewsModel> list4 = new ArrayList<>();

        LatestNewsModel m = new LatestNewsModel();
        m.setDate("Mar 22, 2018");
        m.setTitle("Mother of the Nation Festival");
        list.add(m);
        m = new LatestNewsModel();
        m.setDate("Apr 13, 2018");
        m.setTitle("PAW Patrol Live!");
        list.add(m);
        LatestNewsAdapter latestNewsAdapter = new LatestNewsAdapter(context, R.layout.latest_news_list_item, list);
        holder.allLv.setAdapter(latestNewsAdapter);

        m = new LatestNewsModel();
        m.setDate("Mar 10, 2018");
        m.setTitle("ALPHA 2 Guarantee your Sales/Business/Brand Building.");
        list2.add(m);
        m = new LatestNewsModel();
        m.setDate("Apr 1, 2018");
        m.setTitle("Learn Mandarin Chinese for Free");
        list2.add(m);
        LatestNewsAdapter latestNewsAdapter2 = new LatestNewsAdapter(context, R.layout.latest_news_list_item, list2);
        holder.freelancerLv.setAdapter(latestNewsAdapter2);

        m = new LatestNewsModel();
        m.setDate("Mar 10, 2018");
        m.setTitle("Abu Dhabi Film Commission and twofour54 announce their partnership on ‘Fan of Amoory’ during CABSAT 2018");
        list3.add(m);
        m = new LatestNewsModel();
        m.setDate("Apr 1, 2018");
        m.setTitle("Bollywood star Salman Khan speaks of filming in Abu Dhabi");
        list3.add(m);
        LatestNewsAdapter latestNewsAdapter3 = new LatestNewsAdapter(context, R.layout.latest_news_list_item, list3);
        holder.twofour54Lv.setAdapter(latestNewsAdapter3);

        m = new LatestNewsModel();
        m.setDate("Mar 10, 2018");
        m.setTitle("UAE Exchange Provides Payment Solutions");
        list4.add(m);
        m = new LatestNewsModel();
        m.setDate("Apr 1, 2018");
        m.setTitle("Barzakh Festival - 2018");
        list4.add(m);
        LatestNewsAdapter latestNewsAdapter4 = new LatestNewsAdapter(context, R.layout.latest_news_list_item, list4);
        holder.partenerLv.setAdapter(latestNewsAdapter4);
    }

    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position).getViewType();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public void onPagerItemClick(View view, int position) {
        //on freelancer item list clicked
    }

    private void setTabStyle(TabLayout mTabLayout, int viewType) {

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                text.setTextColor(Color.BLACK);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                text.setTextColor(Color.parseColor("#235FBB"));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        for (int i = 0; i < mTabLayout.getTabCount(); i++) {

            TabLayout.Tab tab = mTabLayout.getTabAt(i);
            if (tab != null) {
                TextView tabTextView = new TextView(context);
                tab.setCustomView(tabTextView);

                tabTextView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
                tabTextView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;

                // center text
                tabTextView.setGravity(Gravity.CENTER);
                // wrap text
                tabTextView.setSingleLine(false);

                tabTextView.setText(tab.getText());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tabTextView.setTextAppearance(R.style.MineCustomTabTextSmall);
                } else
                    tabTextView.setTextAppearance(context, R.style.MineCustomTabTextSmall);

                // First tab is the selected tab, so if i==0 then set BOLD typeface
                if (i == mTabLayout.getTabCount() - 1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tabTextView.setTextAppearance(R.style.MineCustomTabText);
                    } else
                        tabTextView.setTextAppearance(context, R.style.MineCustomTabTextSmall);
                }

                if(i == 0){
                    tabTextView.setTextColor(Color.parseColor("#000000"));
                }
            }
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        //These are the general elements in the RecyclerView
        public TextView title, inboxTitleOne, inboxTitleTwo, inboxCount, briefCount, briefTitleOne, discovrAction;
        //This is the Header on the Recycler (viewType = 0)
        public TextView name, description,aboutBtn,termsBtn,feedbackBtn;
        public View v , partenerAdmin,commissionersAdmin ,userManagerAdmin;
        NonSwipeableViewPager viewPager;
        ListView partenerLv, freelancerLv, allLv, twofour54Lv, industryLv;
        TabLayout mTabLayout;
        GridView resturantsGV;
        SlidingSplashView splashView;
        MaterialSpinner spinner;

        //This constructor would switch what to findViewBy according to the type of viewType
        public ViewHolder(View v, int viewType) {
            super(v);
            this.v = v;
            switch (viewType) {
                case 1:
                case 2:
                    title = (TextView) v.findViewById(R.id.news_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 3:
                    title = (TextView) v.findViewById(R.id.inbox_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    inboxCount = (TextView) v.findViewById(R.id.unread_count);
                    inboxTitleOne = (TextView) v.findViewById(R.id.inbox_title1);
                    inboxTitleTwo = (TextView) v.findViewById(R.id.inbox_title2);
                    break;
                case 4:
                    title = (TextView) v.findViewById(R.id.brief_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    briefCount = (TextView) v.findViewById(R.id.brief_count);
                    briefTitleOne = (TextView) v.findViewById(R.id.brief_title1);
                    break;
                case 5:
                    title = (TextView) v.findViewById(R.id.latest_news_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    mTabLayout = (TabLayout) v.findViewById(R.id.pager_header);
                    viewPager = (NonSwipeableViewPager) v.findViewById(R.id.ln_pager);
                    partenerLv = (ListView) v.findViewById(R.id.partner_lv);
                    freelancerLv = (ListView) v.findViewById(R.id.freelance_lv);
                    allLv = (ListView) v.findViewById(R.id.all_lv);
                    twofour54Lv = (ListView) v.findViewById(R.id.twofour_lv);
                    break;
                case 6:
                    title = (TextView) v.findViewById(R.id.event_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    mTabLayout = (TabLayout) v.findViewById(R.id.pager_header);
                    viewPager = (NonSwipeableViewPager) v.findViewById(R.id.lv_pager);
                    freelancerLv = (ListView) v.findViewById(R.id.freelance_lv);
                    allLv = (ListView) v.findViewById(R.id.all_lv);
                    break;
                case 7:
                    industryLv = (ListView) v.findViewById(R.id.industry_lv);
                    title = (TextView) v.findViewById(R.id.industry_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 8:
                    title = (TextView) v.findViewById(R.id.service_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 9:
                    title = (TextView) v.findViewById(R.id.discover_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    discovrAction = (TextView) v.findViewById(R.id.discover_action);
                    break;
                case 10:
                    title = (TextView) v.findViewById(R.id.offers_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 11:
                    title = (TextView) v.findViewById(R.id.order_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    resturantsGV = (GridView) v.findViewById(R.id.resturants_gv);
                    break;
                case 12:
                    title = (TextView) v.findViewById(R.id.ney_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 14:
                    title = (TextView) v.findViewById(R.id.freelancer_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    splashView = (SlidingSplashView) v.findViewById(R.id.freelancers_sp);
                    break;
                case 15:
                    title = (TextView) v.findViewById(R.id.latest_partners_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    splashView = (SlidingSplashView) v.findViewById(R.id.latest_partnerss_sp);
                    break;
                case 16:
                    title = (TextView) v.findViewById(R.id.mesamix_admin_title);
                    commissionersAdmin = v.findViewById(R.id.commissioners_admin);
                    partenerAdmin = v.findViewById(R.id.partner_admin);
                    userManagerAdmin = v.findViewById(R.id.user_manager_admin);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 17:
                    title = (TextView) v.findViewById(R.id.add_news_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 18:
                    title = (TextView) v.findViewById(R.id.classifieds_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 19:
                    title = (TextView) v.findViewById(R.id.twofour54_serv_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 20:
                    spinner = (MaterialSpinner) v.findViewById(R.id.feedback_spinner);
                    break;
                case 22:
                    aboutBtn = (HoveredTextView)v.findViewById(R.id.about_btn);
                    termsBtn = (HoveredTextView)v.findViewById(R.id.terms_btn);
                    feedbackBtn = (HoveredTextView)v.findViewById(R.id.feedback_btn);
                    break;
                case 32:
                    break;

            }
        }
    }

    class WizardPagerAdapter extends PagerAdapter {

        private final int viewType;
        View v;

        private String[] eventsTabTitles = new String[]{"off campus", "All"};
        private String[] newsTabTitles = new String[]{"freelance\n news", "partner\n news", "twofour54\n news", "All"};

        public WizardPagerAdapter(View view, int viewType) {
            v = view;
            this.viewType = viewType;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (viewType == 5)
                return newsTabTitles[position];
            else return eventsTabTitles[position];
        }

        public Object instantiateItem(ViewGroup collection, int position) {

            int resId = 0;
            if (viewType == 5)
                switch (position) {
                    case 0:
                        resId = R.id.page_one;
                        break;
                    case 1:
                        resId = R.id.page_two;
                        break;
                    case 2:
                        resId = R.id.page_three;
                        break;
                    case 3:
                        resId = R.id.page_four;
                        break;
                }
            else
                switch (position) {
                    case 0:
                        resId = R.id.page_one;
                        break;
                    case 1:
                        resId = R.id.page_two;
                        break;
                }

            return v.findViewById(resId);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            if (viewType == 5)
                return 4;
            else return 2;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }
    }
}