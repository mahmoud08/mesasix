package com.mesasix.connect.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatEditText;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.mesasix.connect.R;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.LatestNewsModel;
import com.mesasix.connect.models.TestModel;
import com.mesasix.connect.views.CenteredImageSpan;
import com.mesasix.connect.views.HoveredTextView;
import com.mesasix.connect.views.HoveredTextViewWithSpan;

import java.util.List;

public class DomainsAdapter extends ArrayAdapter<LatestNewsModel>  implements View.OnClickListener  {

    private final Context context;
    private LayoutInflater lInflater;
    private List<LatestNewsModel> listStorage;
    ListView listView;

    public DomainsAdapter(Context context, int resource, List<LatestNewsModel> items ,ListView listView) {
        super(context, resource, items);
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listView = listView;
        listStorage = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public LatestNewsModel getItem(int position) {
        return listStorage.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = lInflater.inflate(R.layout.domain_dynamic_layout, parent, false);
            holder.removeDomain = (HoveredTextView) convertView.findViewById(R.id.remove_domain);
            holder.domainEd = (AppCompatEditText) convertView.findViewById(R.id.domain_ed);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //holder.domainEd.setError();

        holder.removeDomain.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    remove(getItem(listStorage.size() - 1));
                    Constants.setListViewHeightBasedOnChildren(listView);
                }
                return false;
            }
        });

        return convertView;
    }

    @Override
    public void onClick(View view) {
    }

    static class ViewHolder {
        HoveredTextViewWithSpan title;
        HoveredTextView removeDomain;
        EditText domainEd;
    }
}