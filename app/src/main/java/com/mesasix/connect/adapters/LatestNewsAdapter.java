package com.mesasix.connect.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mesasix.connect.models.LatestNewsModel;
import com.mesasix.connect.R;
import com.mesasix.connect.models.TestModel;

import java.util.List;

public class LatestNewsAdapter extends ArrayAdapter<LatestNewsModel> {

    private final Context context;
    private LayoutInflater lInflater;
    private List<LatestNewsModel> listStorage;

    public LatestNewsAdapter(Context context, int resource, List<LatestNewsModel> items) {
        super(context, resource, items);
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public LatestNewsModel getItem(int position) {
        return listStorage.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateList(List<TestModel> list){

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = lInflater.inflate(R.layout.latest_news_list_item, parent, false);
            holder.title = (TextView) convertView.findViewById(R.id.latest_news_title);
            holder.date = (TextView) convertView.findViewById(R.id.latest_news_date);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(listStorage.get(position).getTitle());
        holder.date.setText(listStorage.get(position).getDate());
        return convertView;
    }

    static class ViewHolder {
        TextView title;
        TextView date;
    }
}