package com.mesasix.connect.models;

public class CropParam {
    public int mAspectX = 0;
    public int mAspectY = 0;
    public int mOutputX = 0;
    public int mOutputY = 0;
    public int mMaxOutputX = 0;
    public int mMaxOutputY = 0;
}