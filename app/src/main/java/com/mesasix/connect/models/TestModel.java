package com.mesasix.connect.models;

/**
 * Created by Esraa.Nayel on 4/12/2018.
 */

public class TestModel {

    private int viewType;
    private String name;

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
