package com.mesasix.connect.models;

/**
 * Created by Esraa.Nayel on 4/12/2018.
 */

public class IndustryNewsModel {

    private String date;
    private String title;
    private String details;
    private String feature;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }
}
