package com.mesasix.connect.models;

/**
 * Created by Esraa.Nayel on 4/12/2018.
 */

public class LatestNewsModel {

    private String date;
    private String title;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
