package com.mesasix.connect.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.AlignmentSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.mesasix.connect.R;
import com.mesasix.connect.fragments.AccountSettingsFragment;
import com.mesasix.connect.fragments.BriefsFragment;
import com.mesasix.connect.fragments.ContactUsFragment;
import com.mesasix.connect.fragments.FreelancersFragment;
import com.mesasix.connect.fragments.HomeFragment;
import com.mesasix.connect.fragments.JobssFragment;
import com.mesasix.connect.fragments.LoginFragment;
import com.mesasix.connect.fragments.NewsEventsFragment;
import com.mesasix.connect.fragments.PartenersFragment;
import com.mesasix.connect.fragments.TermsFragment;
import com.mesasix.connect.models.Constants;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView ui_hot;
    private int hot_number;
    private DrawerLayout drawer;
    private int currentID = -1;

    public void enableDisableDrawer(boolean isEnabled) {
        if (!isEnabled)
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        else
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Html.fromHtml("<font color=\"#575757\">" + getString(R.string.app_name) + "</font>")
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_share);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setBackgroundResource(R.mipmap.floating_icon);
        fab.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#0071B2")));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));

        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        LoginFragment loginFragment = new LoginFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, loginFragment);
        //ft.addToBackStack(null);
        ft.commit();

        //displaySelectedScreen(R.id.nav_home);
    }

    private void displaySelectedScreen(int itemId) {

        //creating fragment object
        Fragment fragment = null;
        String tag = "";
        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_home:
                fragment = new HomeFragment();
                tag = "Home";
                break;
            case R.id.nav_brief:
                fragment = new BriefsFragment();
                tag = "Briefs";
                break;
            case R.id.nav_news_events:
                fragment = new NewsEventsFragment();
                tag = "NewsEvents";
                break;
            case R.id.nav_partner:
                fragment = new PartenersFragment();
                tag = "Parteners";
                break;
            case R.id.nav_freelancer:
                fragment = new FreelancersFragment();
                tag = "Freelancers";
                break;
            case R.id.nav_services:
                fragment = new TermsFragment(Constants.SERVICES_SCREEN);
                tag = "Services";
                break;
            case R.id.nav_jobs:
                fragment = new JobssFragment();
                tag = "Jobs";
                break;
            case R.id.nav_contact:
                fragment = new ContactUsFragment();
                tag = "Contact US";
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment);
            ft.addToBackStack(tag);
            ft.addToBackStack(null);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            /*if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
            }*/

        }
    }

    private Bitmap resizeBitmapImageFn(
            Bitmap bmpSource, int maxResolution){
        int iWidth = bmpSource.getWidth();
        int iHeight = bmpSource.getHeight();
        int newWidth = iWidth ;
        int newHeight = iHeight ;
        float rate = 0.0f;

        if(iWidth > iHeight ){
            if(maxResolution < iWidth ){
                rate = maxResolution / (float) iWidth ;
                newHeight = (int) (iHeight * rate);
                newWidth = maxResolution;
            }
        }else{
            if(maxResolution < iHeight ){
                rate = maxResolution / (float) iHeight ;
                newWidth = (int) (iWidth * rate);
                newHeight = maxResolution;
            }
        }

        return Bitmap.createScaledBitmap(
                bmpSource, newWidth, newHeight, true);
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @SuppressLint("NewApi")
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        final View menu_hotlist = menu.findItem(R.id.aaaa).getActionView();
        ui_hot = (TextView) menu_hotlist.findViewById(R.id.hotlist_hot);
        updateHotCount(10);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint(Html.fromHtml("<font color = #59595B><i>"
                + "search..." + "</i></font>"));
        EditText searchED = (EditText) searchView.
                findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchED.setBackground(getDrawable(R.drawable.white_bg_border));

        Drawable img = getResources().getDrawable(R.drawable.edittext_search);

        img.setColorFilter(Color.parseColor("#59595B"), PorterDuff.Mode.SRC_ATOP);
        searchED.setCompoundDrawables(null, null, img, null);

        // Detect SearchView icon clicks
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setItemsVisibility(menu, searchItem, false);
            }
        });
        // Detect SearchView close
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                setItemsVisibility(menu, searchItem, true);
                return false;
            }
        });

        /*new MyMenuItemStuffListener(menu_hotlist, "Show hot message") {
            @Override
            public void onClick(View v) {
                onHotlistSelected();
            }
        };*/
        final View user = menu.findItem(R.id.username).getActionView();
        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menu.findItem(R.id.username));
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    public void setItemsVisibility(Menu menu, MenuItem exception, boolean visible) {
        for (int i = 0; i < menu.size(); ++i) {
            MenuItem item = menu.getItem(i);
            if (item != exception) item.setVisible(visible);
        }
    }

    // call the updating code on the main thread,
// so we can call this asynchronously
    public void updateHotCount(final int new_hot_number) {
        hot_number = new_hot_number;
        if (ui_hot == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (new_hot_number == 0)
                    ui_hot.setVisibility(View.INVISIBLE);
                else {
                    ui_hot.setVisibility(View.VISIBLE);
                    ui_hot.setText(Integer.toString(new_hot_number));
                }
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        if (id == R.id.settings) {
            AccountSettingsFragment homeFragment = new AccountSettingsFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, homeFragment);
            ft.addToBackStack(null);
            ft.commit();
        }

        if (id == R.id.username) {
            View menuItemView = findViewById(R.id.username); // SAME ID AS MENU ID
           // PopupMenu popupMenu = new PopupMenu(this, menuItemView);
            Context wrapper = new ContextThemeWrapper(this, R.style.popupMenuStyle);
            PopupMenu popupMenu = new PopupMenu(wrapper, menuItemView, Gravity.CENTER);

            popupMenu.inflate(R.menu.popup_menu);
            popupMenu.show();

            final ImageView person = (ImageView) findViewById(R.id.person_icon);
            final ImageView arrow = (ImageView) findViewById(R.id.menu_arrow);

            person.setImageResource(R.drawable.user_icon_selected);
            arrow.setImageResource(R.drawable.down_arrow_selected);

            popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                @Override
                public void onDismiss(PopupMenu popupMenu) {
                    person.setImageResource(R.drawable.user_icon_default);
                    arrow.setImageResource(R.drawable.down_arrow);
                }
            });

            for (int i = 0; i < popupMenu.getMenu().size(); ++i) {
                MenuItem myItem = popupMenu.getMenu().getItem(i);
                SpannableStringBuilder str = new SpannableStringBuilder(myItem.getTitle());
                str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD)
                        , 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                str.setSpan(new RelativeSizeSpan(0.85f), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                str.setSpan(new ForegroundColorSpan(Color.parseColor("#595960")), 0, str.length(), 0);
                str.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), 0, str.length(), 0);
                myItem.setTitle(str);
            }

            MenuItem editItem = popupMenu.getMenu().findItem(R.id.menu_edit_content);
            SpannableString s = new SpannableString(Html.fromHtml("<b>"
                    + "EDIT YOUR CONTENT" + "</b>"));
            s.setSpan(new ForegroundColorSpan(Color.parseColor("#9A9A9A")), 0, s.length(), 0);
            s.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), 0, s.length(), 0);

            editItem.setTitle(s);

            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    if(currentID == item.getItemId())
                        return true;
                    currentID = item.getItemId();
                    switch (item.getItemId()) {
                        case R.id.settings:
                            TermsFragment fragObj = new TermsFragment(Constants.ACCOUNT_SETTINGS);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.user_manag:
                            fragObj = new TermsFragment(Constants.USER_MANAG_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.spam_manag:
                            fragObj = new TermsFragment(Constants.SPAM_MANAG_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.commissioner_manag:
                            fragObj = new TermsFragment(Constants.SPAM_MANAG_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.menu_news:
                            fragObj = new TermsFragment(Constants.NEWS_LISTING_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.menu_events:
                            fragObj = new TermsFragment(Constants.EVENTS_LISTING_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.menu_briefs:
                            fragObj = new TermsFragment(Constants.BRIEF_LISTING_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.menu_jobs:
                            fragObj = new TermsFragment(Constants.JOB_LISTING_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.menu_photos:
                            fragObj = new TermsFragment(Constants.PHOTO_LISTING_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.menu_videos:
                            fragObj = new TermsFragment(Constants.VIDEO_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.menu_classifieds:
                            fragObj = new TermsFragment(Constants.CLASSIFIED_LISTING_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.menu_testimonials:
                            fragObj = new TermsFragment(Constants.TESTIMONIAL_LISTING_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.menu_blogs:
                            fragObj = new TermsFragment(Constants.BLOG_LISTING_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.menu_servicess:
                            fragObj = new TermsFragment(Constants.SERVICE_LISTING_SCREEN);
                            ft.replace(R.id.fragment_container, fragObj);
                            ft.addToBackStack(null);
                            ft.commit();
                            break;
                        case R.id.logout:
                            LoginFragment fragment = new LoginFragment();
                            ft.replace(R.id.fragment_container, fragment);
                            //clear login shared pref
                            ft.commit();
                            break;
                    }
                    return true;
                }
            });

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPanelClosed(int featureId, Menu menu) {
        super.onPanelClosed(featureId, menu);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        displaySelectedScreen(id);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
