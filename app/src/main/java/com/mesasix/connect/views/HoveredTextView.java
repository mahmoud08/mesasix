package com.mesasix.connect.views;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class HoveredTextView extends android.support.v7.widget.AppCompatTextView {

    Rect rect;
    int paintFlag;

    public HoveredTextView(Context context) {
        super(context);
        init();
    }

    public HoveredTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HoveredTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setClickable(true);
        rect = new Rect();
        paintFlag = this.getPaintFlags();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN
                || action == MotionEvent.ACTION_HOVER_ENTER) {
            this.setPaintFlags(this.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            getHitRect(rect);
        } else if (action == MotionEvent.ACTION_MOVE) {
            boolean inView = rect.contains(getLeft() + (int) event.getX(), getTop() + (int) event.getY());
            if (inView)
                this.setPaintFlags(this.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            else
                this.setPaintFlags(paintFlag);
        } else if (action == MotionEvent.ACTION_UP
                || action == MotionEvent.ACTION_CANCEL
                || action == MotionEvent.ACTION_HOVER_EXIT
                || action == MotionEvent.ACTION_OUTSIDE) {
            this.setPaintFlags(paintFlag);
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (focused) {
            this.setPaintFlags(this.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        } else {
            this.setPaintFlags(paintFlag);
        }
    }
}