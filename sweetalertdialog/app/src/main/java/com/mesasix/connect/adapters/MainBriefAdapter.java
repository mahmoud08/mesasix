package com.mesasix.connect.adapters;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chabbal.slidingdotsplash.OnItemClickListener;
import com.mesasix.connect.R;
import com.mesasix.connect.fragments.TermsFragment;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.LatestNewsModel;
import com.mesasix.connect.views.HoveredTextViewWithSpan;

import java.util.List;

public class MainBriefAdapter extends RecyclerView.Adapter<MainBriefAdapter.ViewHolder> implements OnItemClickListener {
    private final Context context;
    private LayoutInflater lInflater;
    private List<LatestNewsModel> listStorage;
    private int mExpandedPosition = -1;

    // Provide a suitable constructor (depends on the kind of dataset)
    public MainBriefAdapter(Context context, List<LatestNewsModel> items) {
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = items;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MainBriefAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_brief_list_item, parent, false);
        return new ViewHolder(v, viewType);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MainBriefAdapter.ViewHolder holder, final int position) {
        holder.title.setSpanSize(25, 10, 2);
        SpannableString title = null;
        title = new SpannableString("Cupiditate dolores laborum deleniti earum nihil in voluptatem. Et ratione voluptas cupiditate quas.    ");

        holder.title.setText(Constants.setSpan(context, title, (int) Constants.dpFromPx(25, context)
                , (int) Constants.dpFromPx(10, context), R.mipmap.if_new_36223, -10));

        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                TermsFragment fragment = new TermsFragment(Constants.BRIEF_SINGLE_ITEM_SCREEN);
                ft.replace(R.id.fragment_container, fragment);
                //clear login shared pref
                ft.addToBackStack(null);
                ft.commit();

            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return listStorage.size();
    }

    @Override
    public void onPagerItemClick(View view, int position) {
        //on freelancer item list clicked
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        HoveredTextViewWithSpan title;

        //This constructor would switch what to findViewBy according to the type of viewType
        public ViewHolder(View v, int viewType) {
            super(v);
            title = (HoveredTextViewWithSpan) v.findViewById(R.id.brief_item_title);
        }
    }
}