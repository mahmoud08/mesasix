package com.mesasix.connect.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mesasix.connect.R;

public class ResturantsAdapter extends BaseAdapter {
    private Context context;
    private final String[] names;

    public ResturantsAdapter(Context context, String[] names) {
        this.context = context;
        this.names = names;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {

            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.resturant_list_item, null);

            // set image based on selected text
            TextView name = (TextView) gridView
                    .findViewById(R.id.resturant_name);
            name.setText(names[position]);

        } else {
            gridView = (View) convertView;
        }

        return gridView;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public String getItem(int position) {
        return names[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}