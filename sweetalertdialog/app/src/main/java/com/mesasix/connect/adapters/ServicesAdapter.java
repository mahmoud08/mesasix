package com.mesasix.connect.adapters;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chabbal.slidingdotsplash.OnItemClickListener;
import com.mesasix.connect.R;
import com.mesasix.connect.fragments.TermsFragment;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.LatestNewsModel;
import com.mesasix.connect.models.ServicesModel;
import com.mesasix.connect.views.HoveredTextViewWithSpan;

import java.util.List;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> implements OnItemClickListener {
    private final Context context;
    private LayoutInflater lInflater;
    private List<ServicesModel> listStorage;
    FragmentTransaction ft;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ServicesAdapter(Context context, List<ServicesModel> items) {
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = items;
         ft = ((AppCompatActivity) context)
                .getSupportFragmentManager().beginTransaction();
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ServicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.eservices_list_item, parent, false);
        return new ViewHolder(v, viewType);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ServicesAdapter.ViewHolder holder, final int position) {
        holder.title.setSpanSize(25,
                25, 1);
        holder.title.setText(Constants.setSpan(context, listStorage.get(position).getTitle(), 25, 25));
        holder.title.setTypeface(Constants.setTitleTypeFace(context));

        holder.desc.setText(listStorage.get(position).getDesc());
        holder.btnTxt.setText(listStorage.get(position).getBtnText());

        holder.img.setImageResource(listStorage.get(position).getImgID());

        //to be removed
        /*if(position == 1 || position==listStorage.size() - 1){
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.btnTxt.getLayoutParams();
            params.height = 160;
            holder.btnTxt.setPadding(20 , 0 ,20 ,0);
            holder.btnTxt.setLayoutParams(params);
        }*/
        ///
        holder.btnTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (position){
                    case 0:
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.SERVICES_INDIVIDUALS_SCREEN));
                        ft.addToBackStack(null);
                        ft.commit();
                        break;
                    case 1:
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.SERVICES_TRAVEL_SCREEN));
                        ft.addToBackStack(null);
                        ft.commit();
                        break;
                    case 3:
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.SERVICES_WELLNES_SCREEN));
                        ft.addToBackStack(null);
                        ft.commit();
                        break;
                    case 5:
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.CLASSIFIEDS_SCREEN));
                        ft.addToBackStack(null);
                        ft.commit();
                        break;
                    case 6:
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.ORDERS_SCREEN));
                        ft.addToBackStack(null);
                        ft.commit();
                        break;
                    case 7:
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.SERVICES_COMPANIES_SCREEN));
                        ft.addToBackStack(null);
                        ft.commit();
                        break;
                    case 8:
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.SERVICES_INTERNSHIP_SCREEN));
                        ft.addToBackStack(null);
                        ft.commit();
                        break;
                    case 9:
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.SERVICES_SEMINAR_SCREEN));
                        ft.addToBackStack(null);
                        ft.commit();
                        break;
                    case 12:
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.SERVICES_OFFICE_SCREEN));
                        ft.addToBackStack(null);
                        ft.commit();
                        break;
                }
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return listStorage.size();
    }

    @Override
    public void onPagerItemClick(View view, int position) {
        //on freelancer item list clicked
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        HoveredTextViewWithSpan title;
        TextView desc;
        AppCompatButton btnTxt;
        ImageView img;

        //This constructor would switch what to findViewBy according to the type of viewType
        public ViewHolder(View v, int viewType) {
            super(v);
            title = (HoveredTextViewWithSpan) v.findViewById(R.id.service_item_name);
            desc = (TextView) v.findViewById(R.id.service_item_brief);
            btnTxt = (AppCompatButton) v.findViewById(R.id.service_item_btn);
            img = (ImageView) v.findViewById(R.id.service_item_img);
        }
    }
}