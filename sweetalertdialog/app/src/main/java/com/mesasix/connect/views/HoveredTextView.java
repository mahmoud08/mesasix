package com.mesasix.connect.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.mesasix.connect.R;
import com.mesasix.connect.fragments.HomeFragment;
import com.mesasix.connect.fragments.TermsFragment;

public class HoveredTextView extends android.support.v7.widget.AppCompatTextView implements View.OnClickListener {

    private  TypedArray typedArray;
    private  int type;
    Rect rect;
    int paintFlag;
    Context context;

    public HoveredTextView(Context context) {
        super(context);
        init(context);
    }

    public HoveredTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
        this.context=context;

        typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomType, 0, 0);
        type = typedArray.getInt(R.styleable.CustomType_typeFace, 0);
    }

    public HoveredTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setClickable(true);
        rect = new Rect();
        paintFlag = this.getPaintFlags();
        this.setOnClickListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN
                || action == MotionEvent.ACTION_HOVER_ENTER) {
            this.setPaintFlags(this.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            getHitRect(rect);
            if(type == 1){
                FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                HomeFragment fragment = new HomeFragment();
                ft.replace(R.id.fragment_container, fragment);
                //clear login shared pref
                ft.addToBackStack(null);
                ft.commit();
            }
        } else if (action == MotionEvent.ACTION_MOVE) {
            boolean inView = rect.contains(getLeft() + (int) event.getX(), getTop() + (int) event.getY());
            if (inView)
                this.setPaintFlags(this.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            else
                this.setPaintFlags(paintFlag);
        } else if (action == MotionEvent.ACTION_UP
                || action == MotionEvent.ACTION_CANCEL
                || action == MotionEvent.ACTION_HOVER_EXIT
                || action == MotionEvent.ACTION_OUTSIDE) {
            this.setPaintFlags(paintFlag);
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (focused) {
            this.setPaintFlags(this.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        } else {
            this.setPaintFlags(paintFlag);
        }
    }

    @Override
    public void onClick(View view) {

    }
}