package com.mesasix.connect.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chabbal.slidingdotsplash.OnItemClickListener;
import com.mesasix.connect.R;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.LatestNewsModel;
import com.mesasix.connect.views.HoveredTextViewWithSpan;

import java.util.List;

public class ClassifiedsAdapter extends RecyclerView.Adapter<ClassifiedsAdapter.ViewHolder> implements OnItemClickListener {
    private final Context context;
    private LayoutInflater lInflater;
    private List<LatestNewsModel> listStorage;
    private int mExpandedPosition = -1;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ClassifiedsAdapter(Context context, List<LatestNewsModel> items) {
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = items;
        this.context = context;
    }

    public static float dpFromPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ClassifiedsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.classifieds_list_item, parent, false);
        return new ViewHolder(v, viewType);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ClassifiedsAdapter.ViewHolder holder, final int position) {
        final boolean isExpanded = position == mExpandedPosition;
        holder.mail.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.mobile.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.postBy.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.details.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.contactDetails.setVisibility(isExpanded ? View.VISIBLE : View.GONE);

        holder.expandItem.setActivated(isExpanded);
        holder.expandItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1 : position;
                notifyItemChanged(position);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return listStorage.size();
    }

    @Override
    public void onPagerItemClick(View view, int position) {
        //on freelancer item list clicked
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView contactDetails;
        TextView title;
        TextView mail;
        TextView expandItem;
        TextView postBy;
        TextView mobile;
        TextView details;

        //This constructor would switch what to findViewBy according to the type of viewType
        public ViewHolder(View v, int viewType) {
            super(v);
            title = (AppCompatTextView) v.findViewById(R.id.item_title);
            details = (AppCompatTextView) v.findViewById(R.id.item_details);
            mail = (AppCompatTextView) v.findViewById(R.id.classifieds_item_email);
            expandItem = (AppCompatTextView) v.findViewById(R.id.expand_item);
            postBy = (AppCompatTextView) v.findViewById(R.id.classifieds_item_post);
            mobile = (AppCompatTextView) v.findViewById(R.id.classifieds_item_mobile);
            contactDetails = (AppCompatTextView) v.findViewById(R.id.contact_details);
            details.setSelected(true);
            details.setEllipsize(android.text.TextUtils.TruncateAt.MARQUEE);
        }
    }
}