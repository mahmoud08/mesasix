package com.mesasix.connect.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mesasix.connect.R;
import com.mesasix.connect.adapters.GeneralAdapter;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.TestModel;

import java.util.ArrayList;
import java.util.List;

public class TermsFragment extends android.support.v4.app.Fragment {

    RecyclerView mRecyclerView;
    private int screenInt;
    private LinearLayoutManager mLayoutManager;

    public TermsFragment() {
    }

    @SuppressLint("ValidFragment")
    public TermsFragment(int screenInt) {
        this.screenInt = screenInt;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        View view = inflater.inflate(R.layout.home_layout, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.news_rv);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.bringToFront();

        // specify an adapter (see also next example)
        List<TestModel> l = new ArrayList<>();
        TestModel mo;
        mo = new TestModel();
        mo.setViewType(1);
        mo.setName("ASDad");
        l.add(mo);

        if (screenInt == Constants.PARTENER_PROFILE_SCREEN)
            for (int i = 11; i < 23; i++) {
                mo = new TestModel();
                mo.setViewType(i);
                mo.setName("ASDad");
                l.add(mo);
            }
        if (screenInt == Constants.FREELANCER_PROFILE_SCREEN)
            for (int i = 10; i < 23; i++) {
                if(i == 11 || i == 12 || i == 13 || i == 14 || i == 15)
                    continue;
                mo = new TestModel();
                mo.setViewType(i);
                mo.setName("ASDad");
                l.add(mo);
            }
        else
            for (int i = 16; i < 23; i++) {
                mo = new TestModel();
                mo.setViewType(i);
                mo.setName("ASDad");
                l.add(mo);
            }

        GeneralAdapter mAdapter = new GeneralAdapter(l, getActivity(), screenInt);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

}