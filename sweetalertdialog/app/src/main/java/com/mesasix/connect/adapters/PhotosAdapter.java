package com.mesasix.connect.adapters;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chabbal.slidingdotsplash.OnItemClickListener;
import com.mesasix.connect.R;
import com.mesasix.connect.fragments.TermsFragment;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.views.HoveredTextView;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.ViewHolder> implements OnItemClickListener {
    private final Context context;
    private LayoutInflater lInflater;
    private List<String> listStorage;
    private int mExpandedPosition = -1;

    // Provide a suitable constructor (depends on the kind of dataset)
    public PhotosAdapter(Context context, List<String> items) {
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = items;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PhotosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.videos_list_item, parent, false);
        return new ViewHolder(v, viewType);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(PhotosAdapter.ViewHolder holder, final int position) {
        Picasso.get().load("https://www.google.com.eg/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png").into(holder.videoImg);

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = ((AppCompatActivity) context)
                        .getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_PHOTO_SCREEN));
                ft.addToBackStack(null);
                ft.commit();
            }
        });

        holder.videoImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent appIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("vnd.youtube:" + listStorage.get(position)));
                Intent webIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.youtube.com/watch?v=" + listStorage.get(position)));
                try {
                    context.startActivity(appIntent);
                } catch (ActivityNotFoundException ex) {
                    context.startActivity(webIntent);
                }*/
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure?")
                        .setContentText("Do you want to delete the video??")
                        .setConfirmText("Yes,delete!")
                        .setCancelText("No,cancel!")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return listStorage.size();
    }

    @Override
    public void onPagerItemClick(View view, int position) {
        //on freelancer item list clicked
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        HoveredTextView title;
        ImageView videoImg, play_icon;
        TextView edit, delete;

        //This constructor would switch what to findViewBy according to the type of viewType
        public ViewHolder(View v, int viewType) {
            super(v);
            title = (HoveredTextView) v.findViewById(R.id.video_item_title);
            videoImg = (ImageView) v.findViewById(R.id.video_item_image);
            play_icon = (ImageView) v.findViewById(R.id.play_icon);
            play_icon.setVisibility(View.GONE);
            videoImg.bringToFront();
            edit = (TextView) v.findViewById(R.id.video_edit);
            delete = (TextView) v.findViewById(R.id.video_delete);
        }
    }
}