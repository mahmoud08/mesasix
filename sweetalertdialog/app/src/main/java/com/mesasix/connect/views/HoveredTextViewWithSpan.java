package com.mesasix.connect.views;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;

import com.mesasix.connect.R;

public class HoveredTextViewWithSpan extends android.support.v7.widget.AppCompatTextView {

    Rect rect;
    int paintFlag;
    Context context;
    private int width = 35 , height = 35;
     int type = 1;

    public HoveredTextViewWithSpan(Context context) {
        super(context);
        init(context);
    }

    public HoveredTextViewWithSpan(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public HoveredTextViewWithSpan(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setClickable(true);
        rect = new Rect();
        paintFlag = this.getPaintFlags();
        this.context = context;
    }

    public static float dpFromPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public void setSpanSize(int width , int height , int type){
        this.width = width;
        this.height = height;
        this.type = type;
    }

    private void setUnselectedSpan(){
        if(type == 2)
            return;
        Drawable myIcon = getResources().getDrawable(R.drawable.arrows_unselected);
        int width = (int) dpFromPx(this.width, context);
        int height = (int) dpFromPx(this.height, context);
        myIcon.setBounds(0, 0, width, height);
        CenteredImageSpan btnFeedback = new CenteredImageSpan(myIcon, ImageSpan.ALIGN_BASELINE);
        SpannableString ssBuilder = new SpannableString(getText());
        ssBuilder.setSpan(
                btnFeedback, // Span to add
                ssBuilder.length() - 1, // Start of the span (inclusive)
                ssBuilder.length(), // End of the span (exclusive)
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);// Do not extend the span when text add later
        setText(ssBuilder);
    }

    private void setSelectedSpan(){
        if(type == 2)
            return;
        Drawable myIcon = getResources().getDrawable( type == 3 ? R.drawable.arrows_selected_green: R.drawable.arrows_selected);
        int width = (int) dpFromPx(this.width, context);
        int height = (int) dpFromPx(this.height, context);
        myIcon.setBounds(0, 0, width, height);
        CenteredImageSpan btnFeedback = new CenteredImageSpan(myIcon, ImageSpan.ALIGN_BASELINE);
        SpannableString ssBuilder = new SpannableString(getText());
        ssBuilder.setSpan(
                btnFeedback, // Span to add
                ssBuilder.length() - 1, // Start of the span (inclusive)
                ssBuilder.length(), // End of the span (exclusive)
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);// Do not extend the span when text add later
        setText(ssBuilder);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN
                || action == MotionEvent.ACTION_HOVER_ENTER) {
            this.setPaintFlags(this.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            setSelectedSpan();
            getHitRect(rect);
        } else if (action == MotionEvent.ACTION_MOVE) {
            boolean inView = rect.contains(getLeft() + (int) event.getX(), getTop() + (int) event.getY());
            if (inView){
                this.setPaintFlags(this.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                setSelectedSpan();
            }
            else{
                this.setPaintFlags(paintFlag);
                setUnselectedSpan();
            }
        } else if (action == MotionEvent.ACTION_UP
                || action == MotionEvent.ACTION_CANCEL
                || action == MotionEvent.ACTION_HOVER_EXIT
                || action == MotionEvent.ACTION_OUTSIDE) {
            this.setPaintFlags(paintFlag);
            setUnselectedSpan();
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (focused) {
            this.setPaintFlags(this.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            setSelectedSpan();
        } else {
            this.setPaintFlags(paintFlag);
            setUnselectedSpan();
        }
    }
}