package com.mesasix.connect.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mesasix.connect.R;
import com.mesasix.connect.activities.MainActivity;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.CropParam;
import com.mesasix.connect.models.CustomDateTimePicker;
import com.mesasix.connect.views.CropImageView;
import com.mesasix.connect.views.CropIntent;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

public class CreateEventsFragment extends XmlFragment {

    private boolean isStart;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = null;

        v = inflater.inflate(R.layout.create_events_fragment, container, false);
       /* v.findViewById(R.id.reset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickReset(view);
            }
        });*/
        v.findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //onClickSave(view);
            }
        });

        final EditText endDateEd = ((EditText) v.findViewById(R.id.end_date_ed));
        final EditText startDateEd = ((EditText) v.findViewById(R.id.start_date_ed));

        final CustomDateTimePicker custom = new CustomDateTimePicker(getActivity(),
                new CustomDateTimePicker.ICustomDateTimeListener() {

                    @Override
                    public void onSet(Dialog dialog, Calendar calendarSelected,
                                      Date dateSelected, int year, String monthFullName,
                                      String monthShortName, int monthNumber, int date,
                                      String weekDayFullName, String weekDayShortName,
                                      int hour24, int hour12, int min, int sec,
                                      String AM_PM) {
                        //                        ((TextInputEditText) findViewById(R.id.edtEventDateTime))
                        if (isStart) {
                            startDateEd.setText("");
                            startDateEd.setText((monthNumber + 1)
                                    + "/" + calendarSelected.get(Calendar.DAY_OF_MONTH) + "/" + year
                                    + " " + hour12 + ":" + min + " " + AM_PM
                            );
                        } else {
                            endDateEd.setText("");
                            endDateEd.setText((monthNumber + 1)
                                    + "/" + calendarSelected.get(Calendar.DAY_OF_MONTH) + "/" + year
                                    + " " + hour12 + ":" + min + " " + AM_PM
                            );
                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                });
        /**
         * Pass Directly current time format it will return AM and PM if you set
         * false
         */
        custom.set24HourFormat(false);
        /**
         * Pass Directly current data and time to show when it pop up
         */
        custom.setDate(Calendar.getInstance());

        startDateEd.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        isStart = true;
                        custom.showDialog();
                    }
                });

        endDateEd.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        isStart = false;
                        custom.showDialog();
                    }
                });

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        PhotoCropFragment fragment = new PhotoCropFragment();
        ft.replace(R.id.photo_fragm_cont, fragment);
        ft.commit();

        return v;
    }

    public void onClickSave(View v) {
    }
}
