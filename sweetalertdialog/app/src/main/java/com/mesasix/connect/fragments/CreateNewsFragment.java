package com.mesasix.connect.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mesasix.connect.R;
import com.mesasix.connect.activities.MainActivity;

public class CreateNewsFragment extends XmlFragment {

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = null;

        v = inflater.inflate(R.layout.create_news_fragment, container, false);
       /* v.findViewById(R.id.reset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickReset(view);
            }
        });*/

        /*final RichEditTextFragment richEditTextFragment =
                (RichEditTextFragment) getChildFragmentManager().findFragmentById(R.id.riched_fragmnt);*/
        //richEditTextFragment.icarus.setContent("<ul><li><b><i><u>Bcvbxbccb vc</u></i></b></li><li><b><i><u>Fxhnncbb</u></i></b></li></ul>");

        v.findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSave(view);
                /*richEditTextFragment.icarus.getContent(new Callback() {
                    @Override
                    public void run(String params) {
                        Log.d("content", params);
                    }
                });*/
            }
        });

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        PhotoCropFragment fragment = new PhotoCropFragment();
        ft.replace(R.id.photo_fragm_cont, fragment);
        ft.commit();

        return v;
    }

    public void onClickSave(View v) {
    }
}