package com.mesasix.connect.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chabbal.slidingdotsplash.OnItemClickListener;
import com.mesasix.connect.R;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.LatestNewsModel;
import com.mesasix.connect.views.HoveredTextViewWithSpan;

import java.util.List;

public class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.ViewHolder> implements OnItemClickListener {
    private final Context context;
    private LayoutInflater lInflater;
    private List<LatestNewsModel> listStorage;
    private int mExpandedPosition = -1;

    // Provide a suitable constructor (depends on the kind of dataset)
    public JobsAdapter(Context context, List<LatestNewsModel> items) {
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = items;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public JobsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v;
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.jobs_list_item, parent, false);
        return new ViewHolder(v, viewType);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(JobsAdapter.ViewHolder holder, final int position) {
        /*holder.title.setSpanSize(25,
                25, 1);
        holder.title.setText(Constants.setSpan(context, "e-services for individuals", 25, 25));*/

        holder.title.setTypeface(Constants.setTitleTypeFace(context));
        holder.title.setSpanSize(35, 12, 2);
        SpannableString title = new SpannableString("OB van crew  ");
        holder.title.setText(Constants.setSpan(context, title, (int) Constants.dpFromPx(35, context)
                , (int) Constants.dpFromPx(12, context), R.mipmap.if_new_36223, -30));

        final boolean isExpanded = position == mExpandedPosition;
        holder.mail.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.startDate.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.postDate.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.timeFrame.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.expandItem.setText(isExpanded ? "read less" : "read more");

        holder.expandItem.setActivated(isExpanded);
        holder.expandItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1 : position;
                notifyItemChanged(position);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return listStorage.size();
    }

    @Override
    public void onPagerItemClick(View view, int position) {
        //on freelancer item list clicked
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        HoveredTextViewWithSpan title;
        TextView mail;
        TextView expandItem;
        TextView startDate;
        TextView timeFrame;
        TextView postDate;
        TextView details;

        //This constructor would switch what to findViewBy according to the type of viewType
        public ViewHolder(View v, int viewType) {
            super(v);
            title = (HoveredTextViewWithSpan) v.findViewById(R.id.item_title);
            details = (AppCompatTextView) v.findViewById(R.id.item_details);
            mail = (AppCompatTextView) v.findViewById(R.id.job_contact_mail);
            expandItem = (AppCompatTextView) v.findViewById(R.id.expand_item);
            startDate = (AppCompatTextView) v.findViewById(R.id.item_start_date);
            timeFrame = (AppCompatTextView) v.findViewById(R.id.item_time_frame);
            postDate = (AppCompatTextView) v.findViewById(R.id.item_post_date);

            details.setSelected(true);
            details.setEllipsize(android.text.TextUtils.TruncateAt.MARQUEE);
        }
    }
}