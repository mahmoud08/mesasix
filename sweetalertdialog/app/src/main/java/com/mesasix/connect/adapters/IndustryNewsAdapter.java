package com.mesasix.connect.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mesasix.connect.R;
import com.mesasix.connect.models.IndustryNewsModel;
import com.mesasix.connect.models.TestModel;
import com.mesasix.connect.views.HoveredTextView;

import java.util.List;

public class IndustryNewsAdapter extends ArrayAdapter<IndustryNewsModel> {

    private final Context context;
    private LayoutInflater lInflater;
    private List<IndustryNewsModel> listStorage;

    public IndustryNewsAdapter(Context context, int resource, List<IndustryNewsModel> items) {
        super(context, resource, items);
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listStorage = items;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listStorage.size();
    }

    @Override
    public IndustryNewsModel getItem(int position) {
        return listStorage.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateList(List<TestModel> list) {

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = lInflater.inflate(R.layout.industry_news_list_item, parent, false);
            holder.title = (TextView) convertView.findViewById(R.id.industry_news_title);
            holder.date = (TextView) convertView.findViewById(R.id.industry_news_date);
            holder.feature = (TextView) convertView.findViewById(R.id.industry_news_feature);
            holder.details = (TextView) convertView.findViewById(R.id.industry_news_details);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(listStorage.get(position).getTitle());
        //holder.date.setText(listStorage.get(position).getDate());
        String text = listStorage.get(position).getDate() + " - " + "<b>" + listStorage.get(position).getFeature() + "</b> " ;
        holder.date.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
        //holder.feature.setText(listStorage.get(position).getFeature());
        holder.details.setText(listStorage.get(position).getDetails());
        return convertView;
    }

    static class ViewHolder {
        TextView title;
        TextView date;
        TextView details;
        TextView feature;
    }
}