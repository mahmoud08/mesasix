package com.mesasix.connect.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.chabbal.slidingdotsplash.OnItemClickListener;
import com.chabbal.slidingdotsplash.OnSetImageListener;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mesasix.connect.R;
import com.mesasix.connect.activities.MainActivity;
import com.mesasix.connect.fragments.TermsFragment;
import com.mesasix.connect.models.Constants;
import com.mesasix.connect.models.ContactUsModel;
import com.mesasix.connect.models.LatestNewsModel;
import com.mesasix.connect.models.ServicesModel;
import com.mesasix.connect.models.TestModel;
import com.mesasix.connect.views.HoveredTextView;
import com.mesasix.connect.views.HoveredTextViewWithSpan;
import com.mesasix.connect.views.NonSwipeableViewPager;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class GeneralAdapter extends RecyclerView.Adapter<GeneralAdapter.ViewHolder> implements OnItemClickListener, View.OnClickListener {
    LinkedHashMap<String, String> companiesList = new LinkedHashMap<String, String>() {
        {
            put("My Applications", "b");
            put("My Contact List", "b");
            put("Create/Edit Contact", "b");
            put("Employment Visa", "b");
            put("Emirates ID", "b");
            put("Health Insurance", "b");
            put("Health Insurance Cancellation", "b");
            put("Change of Health Insurance Plan", "b");
            put("Medical Test", "b");
            put("Change of Visa Status", "b");
            put("Residency Visa (new application)", "b");
            put("Mission Visa", "b");
            put("Visit Visa (for corporate use)", "b");
            put("Cancellation of Visa (for corporate use)", "b");
            put("Residency Visa Renewal", "b");
            put("Visa Transfer In (Within MZA Or Another Government Entity)", "b");
            put("Visa Transfer Out (Within MZA Or Another Government Entity)", "b");
            put("Transfer Visa to Airport", "b");
            put("Change of Visa Details (job title or name change)", "b");
            put("Visa Stamp Transfer to New Passport", "b");
        }
    };
    String[] servicesNames = new String[]{
            "e-services for individuals", "government and travel services",
            "Parking access form",
            "Wellness & Lifestyle",
            "latest travel offers", "classifieds",
            "order food",
            "e-services for companies",
            "do you need an intern?", "Legal Breakfast Seminar",
            "cargo and logistic services", "event venues", "office services"
    };
    String[] servicesDesc = new String[]{
            "Government services for individuals",
            "Your business support partner. Everything you need to settle down comfortably in Abu Dhabi and get your business up and running."
            , "Visitors to partner companies will be able to use the sand parking area for a maximum of two hours; however 24 hours advance notice is required. You will need to complete the below parking access form, sign it then send it to the client relations team."
            , "A better lifestyle is just a few clicks away.",
            "Travel offers exclusive to twofour54 campus community members",
            "Buy, sell, or trade locally... twofour54 connect classifieds offered by and of interest to our partners' community."
            , "Take advantage of the dining and social spaces available on campus.",
            "Government services for companies",
            "twofour54 can help you find the right intern for your company",
            "twofour54 Legal Breakfast Seminar series covers a variety of topics from Protecting Intellectual Property to Employment Law and are designed for twofour54 partners.",
            "As twofour54 strives to always provide you with the most relevant services, we are pleased to introduce Cargo & Logistics services through our travel team.",
            "do you need a venue for your next event? We have few venues that we think you are going to like.",
            "Apply for your office service online!"
    };
    String[] servicesBtnText = new String[]{
            "view e-services",
            "view application forms and contact details"
            , "download parking access form",
            "let's get healthier!", "discover",
            "check it out", "dining orders", "view e-services",
            "let us know!", "read more", "discover", "book it today!",
            "view application forms and contact details"
    };
    int[] servicesImg = new int[]{
            R.mipmap.individuals,
            R.drawable.tawasol,
            R.drawable.parking_banner,
            R.drawable.wellness,
            R.drawable.travel_discover,
            R.drawable.classified_ads,
            R.drawable.fancy_dining,
            R.drawable.companies,
            R.drawable.internship,
            R.drawable.seminar_breakfast,
            R.drawable.cargo,
            R.drawable.venue,
            R.drawable.office_services
    };
    private int screenType;
    private List<TestModel> mDataset;
    private Context context;
    private OnSetImageListener mOnSetImageListener;
    private RecyclerView mRecyclerView;

    // Provide a suitable constructor (depends on the kind of dataset)
    public GeneralAdapter(List<TestModel> myDataset, Context context, int screenType) {
        mDataset = myDataset;
        this.context = context;
        this.screenType = screenType;
    }

    public static float dpFromPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public void setOnShowImageListener(OnSetImageListener onShowImageListener) {
        mOnSetImageListener = onShowImageListener;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        mRecyclerView = recyclerView;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public GeneralAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        View v;
        switch (viewType) {
            case 1:
                switch (screenType) {
                    case Constants.NEWS_EVENTS_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.news_event_screen_view, parent, false);
                        break;
                    case Constants.BRIEF_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.brief_screen_view, parent, false);
                        break;
                    case Constants.PARTENER_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.partener_dir_screen_view, parent, false);
                        break;
                    case Constants.FREELANCER_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.freelancer_dir_screen_view, parent, false);
                        break;
                    case Constants.SERVICES_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.eservices_screen_view, parent, false);
                        break;
                    case Constants.JOBS_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.jobs_screen_view, parent, false);
                        break;
                    case Constants.CONTACT_US_SCREEN:
                    case Constants.FEEDBACK_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.contact_us_screen_view, parent, false);
                        break;
                    case Constants.ABOUT_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.about_screen_view, parent, false);
                        break;
                    case Constants.TERMS_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.terms_screen_view, parent, false);
                        break;
                    case Constants.ACCOUNT_SETTINGS:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.settings_screen_view, parent, false);
                        break;
                    case Constants.INBOX_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.inbox_screen_view, parent, false);
                        break;
                    case Constants.SEND_MESSAGE_ITEM_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.send_message_item_view, parent, false);
                        break;
                    case Constants.ORDERS_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.orders_screen_view, parent, false);
                        break;
                    case Constants.CLASSIFIEDS_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.classifieds_screen_view, parent, false);
                        break;
                    case Constants.USER_MANAG_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.user_mng_screen_view, parent, false);
                        break;
                    case Constants.SPAM_MANAG_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.spam_mng_screen_view, parent, false);
                        break;
                    case Constants.COMMISIONERS_MANAG_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.comm_reg_screen_view, parent, false);
                        break;
                    case Constants.PASS_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.change_pass_screen_view, parent, false);
                        break;
                    case Constants.CREATE_NEWS_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_news_item_view, parent, false);
                        break;
                    case Constants.CREATE_EVENTS_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_event_item_view, parent, false);
                        break;
                    case Constants.CREATE_BRIEF_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_brief_item_view, parent, false);
                        break;
                    case Constants.CREATE_JOB_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_job_item_view, parent, false);
                        break;
                    case Constants.CREATE_PHOTO_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_photo_item_view, parent, false);
                        break;
                    case Constants.CREATE_VIDEO_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_video_item_view, parent, false);
                        break;
                    case Constants.CREATE_TESTIMONIAL_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_testomonial_item_view, parent, false);
                        break;
                    case Constants.CREATE_CLASSIFIED_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_classified_item_view, parent, false);
                        break;
                    case Constants.CREATE_BLOG_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_blog_item_view, parent, false);
                        break;
                    case Constants.CREATE_SERVICE_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_service_item_view, parent, false);
                        break;
                    case Constants.CREATE_COMMISSIONERS_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_commissioners_item_view, parent, false);
                        break;
                    case Constants.CREATE_PARTNER_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_partner_item_view, parent, false);
                        break;
                    case Constants.CREATE_FREELANCER_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_freelancer_item_view, parent, false);
                        break;
                    case Constants.CREATE_PARTNER_USER_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.create_partner_user_item_view, parent, false);
                        break;
                    case Constants.ALL_NEWS_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.all_news_item_view, parent, false);
                        break;
                    case Constants.ALL_EVENTS_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.all_events_item_view, parent, false);
                        break;
                    case Constants.SERVICES_COMPANIES_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.services_for_company_item_view, parent, false);
                        break;
                    case Constants.SERVICES_TRAVEL_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.travel_services_item_view, parent, false);
                        break;
                    case Constants.SERVICES_WELLNES_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.wellness_services_item_view, parent, false);
                        break;
                    case Constants.SERVICES_SEMINAR_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.seminar_services_item_view, parent, false);
                        break;
                    case Constants.SERVICES_INDIVIDUALS_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.services_for_individual_item_view, parent, false);
                        break;
                    case Constants.SERVICES_INTERNSHIP_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.internship_services_item_view, parent, false);
                        break;
                    case Constants.VIDEO_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.video_item_view, parent, false);
                        break;
                    case Constants.PHOTO_LISTING_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.video_item_view, parent, false);
                        break;
                    case Constants.NEWS_LISTING_SCREEN:
                    case Constants.EVENTS_LISTING_SCREEN:
                    case Constants.JOB_LISTING_SCREEN:
                    case Constants.BRIEF_LISTING_SCREEN:
                    case Constants.TESTIMONIAL_LISTING_SCREEN:
                    case Constants.SERVICE_LISTING_SCREEN:
                    case Constants.CLASSIFIED_LISTING_SCREEN:
                    case Constants.BLOG_LISTING_SCREEN:
                    case Constants.COMMISSIONERS_LISTING_SCREEN:
                    case Constants.PARTNER_LISTING_SCREEN:
                    case Constants.FREELANCER_LISTING_SCREEN:
                    case Constants.USER_MANAGEMENT_LISTING_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.listing_item_view, parent, false);
                        break;
                    case Constants.SERVICES_OFFICE_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.office_services_item_view, parent, false);
                        break;
                    case Constants.PARTENER_PROFILE_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.partener_profile_item_view, parent, false);
                        break;
                    case Constants.FREELANCER_PROFILE_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.freelancer_profile_item_view, parent, false);
                        break;
                    case Constants.NEWS_SINGLE_ITEM_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.single_news_item_view, parent, false);
                        break;
                    case Constants.EVENTS_SINGLE_ITEM_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.single_events_item_view, parent, false);
                        break;
                    case Constants.BRIEF_SINGLE_ITEM_SCREEN:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.single_brief_item_view, parent, false);
                        break;
                    default:
                        v = LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.news_event_screen_view, parent, false);
                        break;
                }
                return new ViewHolder(v, viewType);
            case 10:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.freelancer_experience_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 11:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.partener_contact_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 12:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.partener_lnews_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 13:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.partener_brief_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 14:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.partener_twitter_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 15:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.listing_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 16:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.mesasix_admin_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 17:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.add_news_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 18:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.classified_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 19:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.twofour_services_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 20:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.feedback_request_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 21:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.job_item_view, parent, false);
                return new ViewHolder(v, viewType);
            case 22:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.home_footer_view, parent, false);
                return new ViewHolder(v, viewType);
            default:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.news_item_view_1, parent, false);
                return new ViewHolder(v, viewType);
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(GeneralAdapter.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
            case 13:
            case 15:
                setCUListData(holder, holder.getItemViewType());
                /*WizardPagerAdapter adapter = new WizardPagerAdapter(holder.v, holder.getItemViewType());
                holder.viewPager.setAdapter(adapter);
                holder.mTabLayout.setupWithViewPager(holder.viewPager);
                setTabStyle(holder.mTabLayout, holder.getItemViewType());
                holder.viewPager.setOffscreenPageLimit(3);*/
                break;
            case 16:
                holder.commissionersAdmin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragmentTransaction ft = ((AppCompatActivity) context)
                                .getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.COMMISSIONERS_LISTING_SCREEN));
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                });
                holder.partnersAdmin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TermsFragment loginFragment = new TermsFragment(Constants.PARTNER_LISTING_SCREEN);
                        FragmentTransaction ft = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, loginFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                });
                holder.userManagerAdmin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TermsFragment loginFragment = new TermsFragment(Constants.USER_MANAGEMENT_LISTING_SCREEN);
                        FragmentTransaction ft = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, loginFragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                });
                break;
            case 20:
                holder.spinner.setItems("select", "get in touch", "service request", "feedback");
                break;
        }
    }

    private void setTabStyle(TabLayout mTabLayout) {

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                text.setTextColor(Color.BLACK);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView text = (TextView) tab.getCustomView();
                text.setTextColor(Color.parseColor("#235FBB"));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        for (int i = 0; i < mTabLayout.getTabCount(); i++) {

            TabLayout.Tab tab = mTabLayout.getTabAt(i);
            if (tab != null) {
                TextView tabTextView = new TextView(context);
                tab.setCustomView(tabTextView);

                tabTextView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
                tabTextView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;

                tabTextView.setText(tab.getText());

                // center text
                tabTextView.setGravity(Gravity.CENTER);
                // wrap text
                tabTextView.setSingleLine(false);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tabTextView.setTextAppearance(R.style.MineCustomTabText);
                } else
                    tabTextView.setTextAppearance(context, R.style.MineCustomTabText);

                // First tab is the selected tab, so if i==0 then set BOLD typeface
                if (i == mTabLayout.getTabCount() - 1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tabTextView.setTextAppearance(R.style.MineCustomTabText);
                    } else
                        tabTextView.setTextAppearance(context, R.style.MineCustomTabText);
                }
            }
        }
    }

    private void setCUListData(final ViewHolder holder, int position) {
        List<LatestNewsModel> l = new ArrayList<>();
        List<LatestNewsModel> l2 = new ArrayList<>();
        LatestNewsModel model = new LatestNewsModel();
        for (int i = 0; i < 18; i++) {
            model = new LatestNewsModel();
            model.setTitle("Asdad");
            l.add(model);
        }
        if (screenType == Constants.NEWS_EVENTS_SCREEN) {
            NewsAdapter adapter = new NewsAdapter(context, l);
            holder.mRecyclerView.setAdapter(adapter);
            setInnerRv(holder.mRecyclerView);
            model = new LatestNewsModel();
            model.setTitle("Asdad");
            l2.add(model);
            model = new LatestNewsModel();
            model.setTitle("Asdad");
            l2.add(model);
            EventsAdapter eventsAdapter = new EventsAdapter(context, l2);
            holder.eventsRv.setAdapter(eventsAdapter);
            setInnerRv(holder.eventsRv);
            holder.eventTitle.setText(Constants.setSpan(context, "PAW Patrol Live!", 35, 35));

            holder.readAllnewsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.ALL_NEWS_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });

            holder.readAlleventsBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.ALL_EVENTS_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });

        } else if (screenType == Constants.ALL_NEWS_SCREEN) {
            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            NewsAdapter adapter = new NewsAdapter(context, l);
            holder.mRecyclerView.setAdapter(adapter);
            setInnerRv(holder.mRecyclerView);
            setPages(8, holder);
        } else if (screenType == Constants.NEWS_LISTING_SCREEN) {
            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_NEWS_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.EVENTS_LISTING_SCREEN) {
            holder.create.setText("create event");
            holder.title.setText("events");
            holder.listingTitle.setText("events listing");
            holder.allSpinnerParent.setVisibility(View.GONE);
            holder.catgTxt.setVisibility(View.GONE);

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_EVENTS_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.BRIEF_LISTING_SCREEN) {
            holder.create.setText("add new brief");
            holder.title.setText("briefs");
            holder.listingTitle.setText("briefs listing");
            holder.preScreenTitle.setText("briefs");
            holder.allSpinnerParent.setVisibility(View.GONE);
            holder.catgTxt.setVisibility(View.GONE);

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_BRIEF_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.JOB_LISTING_SCREEN) {
            holder.create.setText("new job");
            holder.title.setText("jobs");
            holder.listingTitle.setText("jobs listing");
            holder.preScreenTitle.setText("jobs");
            holder.allSpinnerParent.setVisibility(View.GONE);
            holder.catgTxt.setVisibility(View.GONE);
            holder.srryTxt.setVisibility(View.GONE);
            holder.secondParent.setVisibility(View.VISIBLE);
            holder.hsv.setVisibility(View.VISIBLE);
            holder.pageNumCon.setVisibility(View.VISIBLE);

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_JOB_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.PHOTO_LISTING_SCREEN ||
                (screenType == Constants.PARTENER_PROFILE_SCREEN && position == 15)) {
            /*holder.create.setVisibility(View.GONE);
            holder.create.setText("new photo");
            holder.title.setText("photos");
            holder.listingTitle.setText("photos listing");
            holder.preScreenTitle.setText("partner directory");
            holder.allSpinnerParent.setVisibility(View.GONE);
            holder.catgTxt.setVisibility(View.GONE);
            holder.srryTxt.setVisibility(View.VISIBLE);
            holder.srryTxt.setText("Sorry, no photos found!");
            holder.secondParent.setVisibility(View.GONE);
            holder.hsv.setVisibility(View.GONE);
            holder.pageNumCon.setVisibility(View.GONE);
            if (position == 15)
                holder.topLayout.setVisibility(View.GONE);

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_PHOTO_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });*/
            holder.create.setText("new photo");
            holder.title.setText("photos");
            holder.listingTitle.setText("photos listing");
            List<String> ids = new ArrayList<>();
            ids.add("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASQAAACsCAMAAADlsyHfAAAA+VBMVEUrtlYnMzMMp1AAi1b///8tuVr6/vwnJDAnt1UAo0Wz38OY1K8pr1MoLTIdakAAp0gftE+J0p3O7dcSskkpf0YnLzInKTEAg0fn8OsYf0UApULX7+Fpwot3zY9eq4hYwnbx+vQAjFMdKysoVTsiSTvp9etJv20piUkumm0WZkZEn3ZGUFDx8vKx1cWVx7EAiU8aXEIoTTkWnVYPllYoXD0JoE4nQTYLklYqlk0RcUsNeU8lOTUAqlQFoE0rv1kaoVYoYj4Gg1OE0ZkpdEMqkUsISzUZIiYArjwPQTE7smp+uZ6R0aofdD4dHCgnFy5MqnZrr480hWVCYVaCXJbiAAAIdklEQVR4nO2dC1ecRhSAEYiMCShWB9PYVDdrNNqmZl13NVlF7dr0EdOm/f8/psAuz3lwwWFY3PnOycmJAsN8y9y7dwaIpikUCoVCoVAoFAqFQqFQKBQKhUKhUCgUCoVCoVAoFArFIrMVguQ2J601QbjPdwMOZVlCu1FzkloThfvWDDjckoUZNdd2rysyk/SdNDos6cWmLDosaW/LlUQnJaFfN1dffJF31sG1tLn5TFpzgnB/Xz2UmJM3XpmuK685UWyt7sn7mqTpuimvMXFIlaTra6Ym8TMRRUaS+6xhrgLMK+DGi+QylbT11lxtHmgb5usFil1ZSa9d1CC2rofDTdc3ABtv7S6uJLtBrkej0fW1GfzRS7fVFliSP7Aa5qvpAbb6+2iRJfXxSrO8MSFbeUqSkqQkKUlKEhMlCYCSBEBJAqAkAVCSAChJAJQkAEoSgEdKwp6HseOBd1tGSd54e6T59uW5BdxxCSV5Uw2FeyN0M4HtuXySrG0/PgyylSQq+NxPj4MuPSWJgjPKLvvA9l02SXjgZw+E7iGX0rJJcqb59UPbUpLInmwXJKnhRuIUJanhRoJv8zHpRg03GrkDoamjJFG6khtvNmOr/BGXTtKKp6eW/HP6heQMcj9fPkl4MpqHJYTW6WHbW/f7WUvLJ2kF4wctulPmckB35PQRsseZgy6hpKA7Tn99ejtmzJTgSXBcpGcELqWkQITjYMZeGNvRREqm9l1SSby+Xs4O7W8nlrosydYakGQlXxHQbWypu5KQfmeLlxQktqQVFH8R6K4k2zB6tmhJziD3VXM+vdtdST3DMC6QWEl4YmebQSOn25KGRshQrKT8tGWY4qwOS9r/x5hxLVKSdV+8o3+W4joqafVkLmnnpThJ1oNPtOSHKa6bkjaakOTdko7CFIc7KmknI+lHV4wkPKA2hYIUB5PkfL9Qki5yks4EXUk2/REjNLKgkl4tjiQ0NHKSegciFBUTW6a9+6+dk4RujFqS+JcbmdgyLT6AJf2wIJKujDqS8MEH3my2N2U70jTXhAzoSNKGNA88bKOWpInRO2D31DmnJbYE1+Tsm5ek27wDyeKinqS7YNsJ65f4gN+ma9qAm3QiSWsLYAnNqpGqkryPYaV3xxhwmJXYYlwTsj43l6RLUsEkDNo1JDnHoSOj95Fuybopeb7YNTX/qNRSLKllS2hk1JGEB73Zxr1jmiXvqOwZ7ECS5k/LFsQTSe1aso06kvDYSBiQocWbcoN2SChJQ4w1OoqkVsOSUUtSFLRjxkVLznn5s/zu7A0TxL4sSW1aGtaS5HzKODLuCr/FY0DDM0nIZi2wEJJaG3DozKgjaR60Ez7lBg3GevmFFF9JJXdYZiW1ZClObBUlOR/yjoIUl+1pvH7EZy6pJMXlJLVj6cqoIwkfGEV6mfrEKk1sEbEkjXXzAEVSG5Zso5akFcJRQFJjZNePeCSSNNRnp7iCpBaC90UtSfiOVGQk9YnTB76kJpWk2ewUV5Ak3RIa5nsJlOR87BGCQu6ijoISW0RGEhqBJUkecLmgDZdEBO2YWX3iQRJbREZSvMoEkSTVEtKLvQwknZZKogTtxFJQn8ASW9R+UOCmk2n+NsMSKUmmJbvYx5OT1W8npZLGxd2y9DPP4ZSxt2fu7aX/ZKU4iiSJYak4aE5fhi9+nF1LgaRT+owYI2jHTOEvmiq+ZxJRKkC6JGmWikE7lvTjuxDj5bd3//1E4993XA7hEC/jpE/B0SRJspStRnKS9mcEf/9MY18chKT4RgqAJClhKZlCogy3kHC4/fH5TZHPv51ysR/3Wld0TwnedEkyLBFBO5S0uxp8vLPITQ/cmBu0jTtw8g9B0Yv/8m+RRQ+kJYYkCZaonYwuBoMjaYUftMumawtEl1PhZ/4tMeJYkhoPS0TQLkKVlJ9CIhgKeWUmkVNZkhq2RAnaEEneMeOb9owLMedmF9eEmZIaHXBENQKTxKxG5og6u5EFldSkJaIaAUnCB3xHwi5+dORBJTVniZbYAJIm/KDNvHmkOmhqQSU1ZumC21mWpJJqpGJi45NfZeJKaiZ4k9UISBJrCmmOmMSWkp2C40pqxBIkaFMklQRtMYktc5bZVSa+pAYGHDmFBJKULGgzEP5xohsPKkm8JVDQJiVN+BtfCT/PIMVZUEnCLfEvCJYkmUE7xk+m4EolibUEDNpFSc4nrtuzZl7gn6wylUsSOdoB1QhNUnFBu8BQ4Bnmmae4ckkCLdGmkACScJ/rqCfs/IjznT+uC5AkbsCBg3ZOUskUkvjEljK/kQIiSZQlGxy0c5ImRo9N4KjefzYBwz9yoJLEWEKgaoSQhPvHPNa3m2UMliTCUpWgnbuSMBenYcDDTUTwhlYjpKT2AUp6vCVoNdJlSY8dcJUSWyrJkTu2GFhQSY+0VC1ox5KOP+RZb4l7qKTHWKpQjWQl7RQWHs82WmINLKl+WKoctGeS3v9S4EVrfNkHSqptqVI1knD61/sCz9vkT5ij2gOuetCeWdrJs9YqUEd1LVWqRpiM4KfZMjUU1QnaFM7a7noFqjuqWI0wGLbd8SpUDd5oxJ94BXLRdr+rUdHSVbkAAL22e12VapKEOOpQ0I6p4qh6NfI0HFWwJChodymxpUAd1alGSDqV2FKAwbvyFBKVjiW2FJClmtVIkbb7Wh+IpOUN2jGligRVIzdtd/RRlDkSE7S7mdhS+I7EBO2OJrYUbvAWE7Q7V42Q8CyJmUJqu4ciYA82MUG7y4ktheVITDXS7cSWQnekElsemiQxU0idrUZIaMFbiKMnkNhSSEdiqpG2+yUWldggNBG0n0piS8k6UtUIg0zwFlONPKHElpJaElKNPKnEliI2aLfdm6YQGbSfWmJLCSXVuguJ4OklthTtfy9tITG8r50zAAAAAElFTkSuQmCC");
            ids.add("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASQAAACsCAMAAADlsyHfAAAA+VBMVEUrtlYnMzMMp1AAi1b///8tuVr6/vwnJDAnt1UAo0Wz38OY1K8pr1MoLTIdakAAp0gftE+J0p3O7dcSskkpf0YnLzInKTEAg0fn8OsYf0UApULX7+Fpwot3zY9eq4hYwnbx+vQAjFMdKysoVTsiSTvp9etJv20piUkumm0WZkZEn3ZGUFDx8vKx1cWVx7EAiU8aXEIoTTkWnVYPllYoXD0JoE4nQTYLklYqlk0RcUsNeU8lOTUAqlQFoE0rv1kaoVYoYj4Gg1OE0ZkpdEMqkUsISzUZIiYArjwPQTE7smp+uZ6R0aofdD4dHCgnFy5MqnZrr480hWVCYVaCXJbiAAAIdklEQVR4nO2dC1ecRhSAEYiMCShWB9PYVDdrNNqmZl13NVlF7dr0EdOm/f8/psAuz3lwwWFY3PnOycmJAsN8y9y7dwaIpikUCoVCoVAoFAqFQqFQKBQKhUKhUCgUCoVCoVAoFArFIrMVguQ2J601QbjPdwMOZVlCu1FzkloThfvWDDjckoUZNdd2rysyk/SdNDos6cWmLDosaW/LlUQnJaFfN1dffJF31sG1tLn5TFpzgnB/Xz2UmJM3XpmuK685UWyt7sn7mqTpuimvMXFIlaTra6Ym8TMRRUaS+6xhrgLMK+DGi+QylbT11lxtHmgb5usFil1ZSa9d1CC2rofDTdc3ABtv7S6uJLtBrkej0fW1GfzRS7fVFliSP7Aa5qvpAbb6+2iRJfXxSrO8MSFbeUqSkqQkKUlKEhMlCYCSBEBJAqAkAVCSAChJAJQkAEoSgEdKwp6HseOBd1tGSd54e6T59uW5BdxxCSV5Uw2FeyN0M4HtuXySrG0/PgyylSQq+NxPj4MuPSWJgjPKLvvA9l02SXjgZw+E7iGX0rJJcqb59UPbUpLInmwXJKnhRuIUJanhRoJv8zHpRg03GrkDoamjJFG6khtvNmOr/BGXTtKKp6eW/HP6heQMcj9fPkl4MpqHJYTW6WHbW/f7WUvLJ2kF4wctulPmckB35PQRsseZgy6hpKA7Tn99ejtmzJTgSXBcpGcELqWkQITjYMZeGNvRREqm9l1SSby+Xs4O7W8nlrosydYakGQlXxHQbWypu5KQfmeLlxQktqQVFH8R6K4k2zB6tmhJziD3VXM+vdtdST3DMC6QWEl4YmebQSOn25KGRshQrKT8tGWY4qwOS9r/x5hxLVKSdV+8o3+W4joqafVkLmnnpThJ1oNPtOSHKa6bkjaakOTdko7CFIc7KmknI+lHV4wkPKA2hYIUB5PkfL9Qki5yks4EXUk2/REjNLKgkl4tjiQ0NHKSegciFBUTW6a9+6+dk4RujFqS+JcbmdgyLT6AJf2wIJKujDqS8MEH3my2N2U70jTXhAzoSNKGNA88bKOWpInRO2D31DmnJbYE1+Tsm5ek27wDyeKinqS7YNsJ65f4gN+ma9qAm3QiSWsLYAnNqpGqkryPYaV3xxhwmJXYYlwTsj43l6RLUsEkDNo1JDnHoSOj95Fuybopeb7YNTX/qNRSLKllS2hk1JGEB73Zxr1jmiXvqOwZ7ECS5k/LFsQTSe1aso06kvDYSBiQocWbcoN2SChJQ4w1OoqkVsOSUUtSFLRjxkVLznn5s/zu7A0TxL4sSW1aGtaS5HzKODLuCr/FY0DDM0nIZi2wEJJaG3DozKgjaR60Ez7lBg3GevmFFF9JJXdYZiW1ZClObBUlOR/yjoIUl+1pvH7EZy6pJMXlJLVj6cqoIwkfGEV6mfrEKk1sEbEkjXXzAEVSG5Zso5akFcJRQFJjZNePeCSSNNRnp7iCpBaC90UtSfiOVGQk9YnTB76kJpWk2ewUV5Ak3RIa5nsJlOR87BGCQu6ijoISW0RGEhqBJUkecLmgDZdEBO2YWX3iQRJbREZSvMoEkSTVEtKLvQwknZZKogTtxFJQn8ASW9R+UOCmk2n+NsMSKUmmJbvYx5OT1W8npZLGxd2y9DPP4ZSxt2fu7aX/ZKU4iiSJYak4aE5fhi9+nF1LgaRT+owYI2jHTOEvmiq+ZxJRKkC6JGmWikE7lvTjuxDj5bd3//1E4993XA7hEC/jpE/B0SRJspStRnKS9mcEf/9MY18chKT4RgqAJClhKZlCogy3kHC4/fH5TZHPv51ysR/3Wld0TwnedEkyLBFBO5S0uxp8vLPITQ/cmBu0jTtw8g9B0Yv/8m+RRQ+kJYYkCZaonYwuBoMjaYUftMumawtEl1PhZ/4tMeJYkhoPS0TQLkKVlJ9CIhgKeWUmkVNZkhq2RAnaEEneMeOb9owLMedmF9eEmZIaHXBENQKTxKxG5og6u5EFldSkJaIaAUnCB3xHwi5+dORBJTVniZbYAJIm/KDNvHmkOmhqQSU1ZumC21mWpJJqpGJi45NfZeJKaiZ4k9UISBJrCmmOmMSWkp2C40pqxBIkaFMklQRtMYktc5bZVSa+pAYGHDmFBJKULGgzEP5xohsPKkm8JVDQJiVN+BtfCT/PIMVZUEnCLfEvCJYkmUE7xk+m4EolibUEDNpFSc4nrtuzZl7gn6wylUsSOdoB1QhNUnFBu8BQ4Bnmmae4ckkCLdGmkACScJ/rqCfs/IjznT+uC5AkbsCBg3ZOUskUkvjEljK/kQIiSZQlGxy0c5ImRo9N4KjefzYBwz9yoJLEWEKgaoSQhPvHPNa3m2UMliTCUpWgnbuSMBenYcDDTUTwhlYjpKT2AUp6vCVoNdJlSY8dcJUSWyrJkTu2GFhQSY+0VC1ox5KOP+RZb4l7qKTHWKpQjWQl7RQWHs82WmINLKl+WKoctGeS3v9S4EVrfNkHSqptqVI1knD61/sCz9vkT5ij2gOuetCeWdrJs9YqUEd1LVWqRpiM4KfZMjUU1QnaFM7a7noFqjuqWI0wGLbd8SpUDd5oxJ94BXLRdr+rUdHSVbkAAL22e12VapKEOOpQ0I6p4qh6NfI0HFWwJChodymxpUAd1alGSDqV2FKAwbvyFBKVjiW2FJClmtVIkbb7Wh+IpOUN2jGligRVIzdtd/RRlDkSE7S7mdhS+I7EBO2OJrYUbvAWE7Q7V42Q8CyJmUJqu4ciYA82MUG7y4ktheVITDXS7cSWQnekElsemiQxU0idrUZIaMFbiKMnkNhSSEdiqpG2+yUWldggNBG0n0piS8k6UtUIg0zwFlONPKHElpJaElKNPKnEliI2aLfdm6YQGbSfWmJLCSXVuguJ4OklthTtfy9tITG8r50zAAAAAElFTkSuQmCC");
            PhotosAdapter adapter = new PhotosAdapter(context, ids);
            holder.mRecyclerView.setAdapter(adapter);
            setInnerRv(holder.mRecyclerView);
            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_PHOTO_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } /*else if (screenType == Constants.VIDEO_LISTING_SCREEN) {
            holder.create.setText("new video");
            holder.title.setText("videos");
            holder.listingTitle.setText("videos listing");
            holder.preScreenTitle.setText("partner directory");
            holder.allSpinnerParent.setVisibility(View.GONE);
            holder.catgTxt.setVisibility(View.GONE);
            holder.srryTxt.setVisibility(View.VISIBLE);
            holder.srryTxt.setText("Sorry, no videos found!");
            holder.secondParent.setVisibility(View.GONE);
            holder.hsv.setVisibility(View.GONE);
            holder.pageNumCon.setVisibility(View.GONE);

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_VIDEO_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        }*/ else if (screenType == Constants.TESTIMONIAL_LISTING_SCREEN) {
            holder.create.setText("new testimonial");
            holder.title.setText("testimonials");
            holder.listingTitle.setText("testimonials listing");
            holder.preScreenTitle.setText("briefing room");
            holder.allSpinnerParent.setVisibility(View.GONE);
            holder.catgTxt.setVisibility(View.GONE);
            holder.srryTxt.setVisibility(View.GONE);
            holder.allSpinnerParent.setVisibility(View.VISIBLE);
            //holder.srryTxt.setText("Sorry, no testimonials found!");
            holder.secondParent.setVisibility(View.VISIBLE);
            holder.hsv.setVisibility(View.VISIBLE);
            holder.pageNumCon.setVisibility(View.VISIBLE);

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_TESTIMONIAL_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.CLASSIFIED_LISTING_SCREEN) {
            holder.create.setText("add new classifieds");
            holder.title.setText("classifieds");
            holder.listingTitle.setText("classifieds listing");
            holder.preScreenTitle.setText("classifieds");
            holder.allSpinnerParent.setVisibility(View.GONE);
            holder.catgTxt.setVisibility(View.GONE);
            holder.srryTxt.setVisibility(View.GONE);
            //holder.srryTxt.setText("Sorry, no classifieds posted yet!");
            holder.secondParent.setVisibility(View.VISIBLE);
            holder.hsv.setVisibility(View.VISIBLE);
            holder.pageNumCon.setVisibility(View.VISIBLE);

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_CLASSIFIED_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.SERVICE_LISTING_SCREEN) {
            holder.create.setText("create new service");
            holder.title.setText("services");
            holder.listingTitle.setText("services listing");
            holder.preScreenTitle.setText("cms");
            holder.allSpinnerParent.setVisibility(View.GONE);
            holder.catgTxt.setVisibility(View.GONE);
            holder.secondParent.setVisibility(View.GONE);
            holder.pageNumCon.setVisibility(View.GONE);

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_SERVICE_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.BLOG_LISTING_SCREEN) {
            holder.create.setText("create new blog");
            holder.title.setText("blogs");
            holder.listingTitle.setText("blogs listing");
            holder.preScreenTitle.setText("blogs");
            holder.catgTxt.setVisibility(View.GONE);
            holder.srryTxt.setVisibility(View.GONE);
            holder.allSpinnerParent.setVisibility(View.GONE);
            //holder.srryTxt.setText("Sorry, no blogs posted yet!");
            holder.secondParent.setVisibility(View.VISIBLE);
            holder.hsv.setVisibility(View.VISIBLE);
            holder.pageNumCon.setVisibility(View.VISIBLE);

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_BLOG_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.COMMISSIONERS_LISTING_SCREEN) {
            holder.create.setText("new content commissioner");
            holder.title.setText("content commissioners");
            holder.listingTitle.setText("content commissioner listing");
            holder.preScreenTitle.setText("partner directory");
            holder.catgTxt.setVisibility(View.GONE);
            holder.allSpinnerParent.setVisibility(View.GONE);
            /*holder.srryTxt.setVisibility(View.VISIBLE);
            holder.srryTxt.setText("Sorry, no blogs posted yet!");
            holder.secondParent.setVisibility(View.GONE);
            holder.hsv.setVisibility(View.GONE);
            holder.pageNumCon.setVisibility(View.GONE);*/

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_COMMISSIONERS_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.PARTNER_LISTING_SCREEN) {
            holder.create.setText("new partner");
            holder.title.setText("partners");
            holder.listingTitle.setText("partner listing");
            holder.preScreenTitle.setText("partner directory");
            holder.allSpinnerParent.setVisibility(View.GONE);
            holder.catgTxt.setVisibility(View.GONE);
            holder.secondParent.setVisibility(View.VISIBLE);

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_PARTNER_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.FREELANCER_LISTING_SCREEN) {
            holder.create.setText("add freelancer");
            holder.title.setText("freelancers");
            holder.listingTitle.setText("freelancer listing");
            holder.preScreenTitle.setText("freelancer directory");
            holder.allSpinnerParent.setVisibility(View.GONE);
            holder.catgTxt.setVisibility(View.GONE);
            holder.secondParent.setVisibility(View.VISIBLE);

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* Fragment f = ((AppCompatActivity) context).getSupportFragmentManager()
                            .findFragmentByTag("159357");
                    String backStateName = f.getClass().getName();
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    boolean fragmentPopped = ((AppCompatActivity) context)
                            .getSupportFragmentManager().popBackStackImmediate (backStateName, 0);

                    if (!fragmentPopped){ //fragment not in back stack, create it.
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_FREELANCER_SCREEN),"testtt");
                        ft.addToBackStack(null);
                        ft.commit();
                    }*/
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_FREELANCER_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.USER_MANAGEMENT_LISTING_SCREEN) {
            holder.create.setText("add user");
            holder.title.setText("user management");
            holder.listingTitle.setText("user management");
            holder.preScreenTitle.setVisibility(View.GONE);
            holder.titleArrow.setVisibility(View.GONE);
            holder.catgTxt.setText("show");
            holder.rd.setVisibility(View.VISIBLE);
            holder.exportContainer.setVisibility(View.VISIBLE);
            holder.secondParent.setVisibility(View.VISIBLE);
            holder.allSpinner.setText("select user type");
            holder.allSpinner.setItems("select user type", "freelancer", "partner", "content commissioner");

            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("Asdad");
                l.add(model);
            }
            ItemRowAdapter adapter = new ItemRowAdapter(context, l);
            holder.newslistingRv.setAdapter(adapter);
            setInnerRv(holder.newslistingRv);
            setPages(8, holder);

            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_PARTNER_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });

            holder.exportArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Rect r = Constants.locateView(view);
                    LayoutInflater lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View popup_view = lInflater.inflate(R.layout.popup_layout, null);
                    final PopupWindow popup = new PopupWindow(popup_view, 500, FrameLayout.LayoutParams.WRAP_CONTENT, true);
                    popup.setFocusable(true);
                    popup.setBackgroundDrawable(new ColorDrawable());
                    popup.showAtLocation(view, Gravity.TOP | Gravity.CENTER, r.centerX() - 715, r.bottom + 40);
                    ((TextView) popup_view.findViewById(R.id.first_txt)).setText("add partner\nuser");
                    ((TextView) popup_view.findViewById(R.id.second_txt)).setText("add content\ncommissioner");
                    ((TextView) popup_view.findViewById(R.id.third_txt)).setText("add freelancer");
                    popup_view.findViewById(R.id.third_txt).setVisibility(View.VISIBLE);
                    popup_view.findViewById(R.id.second_txt).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            FragmentTransaction ft = ((AppCompatActivity) context)
                                    .getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_COMMISSIONERS_SCREEN));
                            ft.addToBackStack(null);
                            ft.commit();
                            popup.dismiss();
                        }
                    });
                    popup_view.findViewById(R.id.third_txt).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            FragmentTransaction ft = ((AppCompatActivity) context)
                                    .getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.fragment_container,
                                    new TermsFragment(Constants.CREATE_FREELANCER_SCREEN), "159357");
                            ft.addToBackStack(null);
                            ft.commit();
                            popup.dismiss();
                        }
                    });
                    popup_view.findViewById(R.id.first_txt).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            FragmentTransaction ft = ((AppCompatActivity) context)
                                    .getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_PARTNER_USER_SCREEN));
                            ft.addToBackStack(null);
                            ft.commit();
                            popup.dismiss();
                        }
                    });
                }
            });
        } else if (screenType == Constants.CREATE_FREELANCER_SCREEN) {
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.FREELANCER_LISTING_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.CREATE_PARTNER_USER_SCREEN) {
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.USER_MANAGEMENT_LISTING_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.CREATE_COMMISSIONERS_SCREEN) {
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.COMMISSIONERS_LISTING_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.CREATE_PARTNER_SCREEN) {
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.PARTNER_LISTING_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.CREATE_BLOG_SCREEN) {
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.BLOG_LISTING_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.CREATE_SERVICE_SCREEN) {
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.SERVICE_LISTING_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.CREATE_VIDEO_SCREEN) {
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.VIDEO_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.CREATE_PHOTO_SCREEN) {
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.PHOTO_LISTING_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.CREATE_NEWS_SCREEN) {
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.NEWS_LISTING_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.CREATE_BRIEF_SCREEN) {
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.BRIEF_LISTING_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.CREATE_JOB_SCREEN) {

        } else if (screenType == Constants.CREATE_EVENTS_SCREEN) {
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.EVENTS_LISTING_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        }

       /* else if(screenType == Constants.CREATE_BRIEF_SCREEN){
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.BRIEF_SCREEN));
                    ft.addToBackStack("BRIEF SCREEN");
                    ft.commit();
                }
            });
        }
*/
        else if (screenType == Constants.JOBS_SCREEN) {
            JobsAdapter adapter = new JobsAdapter(context, l);
            holder.mRecyclerView.setAdapter(adapter);
            setInnerRv(holder.mRecyclerView);
            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_JOB_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        } else if (screenType == Constants.SERVICES_OFFICE_SCREEN) {
            holder.accessCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Constants.openLinks(context, "https://twofour54.formstack.com/forms/temporary_access_card");
                }
            });
            holder.replaceCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Constants.openLinks(context, "https://twofour54.formstack.com/forms/id_accesscard_replacement");
                }
            });
            holder.booking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Constants.openLinks(context, "https://twofour54.formstack.com/forms/customer_care_meeting_room_booking");
                }
            });
        } else if (screenType == Constants.ALL_EVENTS_SCREEN) {
            l.clear();
            for (int i = 0; i < 10; i++) {
                model = new LatestNewsModel();
                model.setTitle("ALL_EVENTS_SCREEN");
                l.add(model);
            }
            EventsAdapter adapter = new EventsAdapter(context, l);
            holder.mRecyclerView.setAdapter(adapter);
            setInnerRv(holder.mRecyclerView);
            setPages(8, holder);

        } else if (screenType == Constants.SERVICES_COMPANIES_SCREEN) {
            setCompanies(companiesList.size(), holder);

        } else if (screenType == Constants.BRIEF_SCREEN || position == 13) {
            BriefsAdapter adapter = new BriefsAdapter(context, R.layout.brief_list_item, l, false);
            holder.lv.setAdapter(adapter);
            try {
                holder.monthsSpinner.setDropdownMaxHeight(500);
                holder.yearsSpinner.setItems("year", "2018");
                holder.ordersSpinner.setItems("newest first", "oldest first");
                holder.monthsSpinner.setItems("month", "January", "February", "March", "April",
                        "May", "June", "July", "August", "September"
                        , "October", "November", "December");
                setPages(8, holder);

                holder.create.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragmentTransaction ft = ((AppCompatActivity) context)
                                .getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_BRIEF_SCREEN));
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                });
            } catch (Exception e) {
            }

        } else if (screenType == Constants.PARTENER_SCREEN) {
            ParDirAdapter adapter = new ParDirAdapter(context, l);
            holder.mRecyclerView.setAdapter(adapter);
            holder.categSpinner.setDropdownMaxHeight(800);
            holder.categSpinner.setItems("select category", "Production & Services",
                    "Aniamtion & Special FX", "Bussiness Support Service", "Events", "Marketing", "Gaming");
            //holder.categSpinner
            setInnerRv(holder.mRecyclerView);


        } else if (screenType == Constants.FREELANCER_SCREEN) {
            FreelancersDirAdapter adapter = new FreelancersDirAdapter(context, l);
            holder.mRecyclerView.setAdapter(adapter);
            holder.skillsSpinner.setDropdownMaxHeight(800);
            holder.skillsSpinner.setItems("select skillset", "Actor", "Animator", "Developer", "IT", "Photoghraper"
                    , "Singer", "Cooker", "Engineer");
            setInnerRv(holder.mRecyclerView);
        } else if (screenType == Constants.SERVICES_SCREEN) {
            List<ServicesModel> list = new ArrayList<>();
            for (int i = 0; i < servicesBtnText.length; i++) {
                ServicesModel servicesModel = new ServicesModel();
                servicesModel.setTitle(servicesNames[i]);
                servicesModel.setDesc(servicesDesc[i]);
                servicesModel.setBtnText(servicesBtnText[i]);
                servicesModel.setImgID(servicesImg[i]);
                list.add(servicesModel);
            }

            ((HoveredTextViewWithSpan) holder.serviceFeedBackTxt).setSpanSize(35, 35, 1);
            holder.serviceFeedBackTxt.setText(Constants.setSpan(context,
                    "we want your feedback!", 35, 35));
            ServicesAdapter adapter = new ServicesAdapter(context, list);
            holder.mRecyclerView.setAdapter(adapter);
            setInnerRv(holder.mRecyclerView);

        } else if (screenType == Constants.CONTACT_US_SCREEN) {

            List<ContactUsModel> list = new ArrayList<>();
            ContactUsModel contactUsModel = new ContactUsModel();
            contactUsModel.setName("Client Relationship Team");
            contactUsModel.setDesc("for general enquiries");
            contactUsModel.setMail("client.relationship@twofour54.com");
            contactUsModel.setMobile("t +971 2 401 2454 e");
            list.add(contactUsModel);

            contactUsModel = new ContactUsModel();
            contactUsModel.setName("Business Development");
            contactUsModel.setDesc("for business related enquiries");
            contactUsModel.setMail("businessdevelopment@twofour54.com");
            contactUsModel.setMobile("t +971 2 401 2454 e");
            list.add(contactUsModel);

            contactUsModel = new ContactUsModel();
            contactUsModel.setName("Government & Travel Services");
            contactUsModel.setDesc("for government services");
            contactUsModel.setMobile("t 800 2454 e");
            contactUsModel.setMail("customercare@twofour54.com");
            list.add(contactUsModel);

            contactUsModel = new ContactUsModel();
            contactUsModel.setName("Digital Team");
            contactUsModel.setDesc("portal related issues");
            contactUsModel.setMail("connect@twofour54.com");
            contactUsModel.setMobile("t +971 2 401 2454 e");
            list.add(contactUsModel);

            ContactUsAdapter adapter = new ContactUsAdapter(context, R.layout.brief_list_item, list);
            holder.lv.setAdapter(adapter);
            WizardPagerAdapter wizardPagerAdapter = new WizardPagerAdapter(holder.v, holder.getItemViewType());
            holder.viewPager.setAdapter(wizardPagerAdapter);
            holder.mTabLayout.setupWithViewPager(holder.viewPager);
            setTabStyle(holder.mTabLayout);
            holder.viewPager.setOffscreenPageLimit(3);
        } else if (screenType == Constants.ORDERS_SCREEN) {
            holder.costaTxt.setOnClickListener(this);
            holder.nathalieTxt.setOnClickListener(this);
            holder.subwayTxt.setOnClickListener(this);
            holder.starbucksTxt.setOnClickListener(this);
            holder.foodonclickTxt.setOnClickListener(this);
            holder.talabatTxt.setOnClickListener(this);
            holder.h24Txt.setOnClickListener(this);
            holder.zomatoTxt.setOnClickListener(this);
        } else if (screenType == Constants.CLASSIFIEDS_SCREEN) {

            ClassifiedsAdapter adapter = new ClassifiedsAdapter(context, l);
            holder.mRecyclerView.setAdapter(adapter);
            setInnerRv(holder.mRecyclerView);
            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_CLASSIFIED_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });

        } else if (screenType == Constants.VIDEO_SCREEN) {
            List<String> ids = new ArrayList<>();
            ids.add("AE9oLxhSXig");
            ids.add("vZsNDQOa2ao");
            VideosAdapter adapter = new VideosAdapter(context, ids);
            holder.mRecyclerView.setAdapter(adapter);
            setInnerRv(holder.mRecyclerView);
            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.CREATE_VIDEO_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });

        } else if (screenType == Constants.USER_MANAG_SCREEN) {
            final List<LatestNewsModel> aa = new ArrayList<>();
            aa.add(new LatestNewsModel());
            final DomainsAdapter adapter = new DomainsAdapter(context, 0, aa, holder.lv);
            holder.lv.setAdapter(adapter);
            holder.addDomain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    aa.add(new LatestNewsModel());
                    //adapter.add(new LatestNewsModel());
                    Constants.setListViewHeightBasedOnChildren(holder.lv);
                }
            });
        } else if (screenType == Constants.PARTENER_PROFILE_SCREEN) {
            holder.partnerProfileTitle
                    .setText(Constants.setSpan(context, "mesasix.com", 30, 30));
            holder.parContDet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float y = mRecyclerView.getChildAt(1).getTop()
                            - mRecyclerView.getChildAt(0).getTop();
                    ((NestedScrollView) mRecyclerView.getParent()).smoothScrollTo(0, (int) y);
                }
            });
            holder.parLNews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float y = mRecyclerView.getChildAt(2).getTop();
                    ((NestedScrollView) mRecyclerView.getParent()).smoothScrollTo(0, (int) y);
                }
            });
            holder.parBrief.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float y = mRecyclerView.getChildAt(3).getTop();
                    ((NestedScrollView) mRecyclerView.getParent()).smoothScrollTo(0, (int) y);
                }
            });
            holder.parTweets.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float y = mRecyclerView.getChildAt(4).getTop();
                    ((NestedScrollView) mRecyclerView.getParent()).smoothScrollTo(0, (int) y);
                }
            });

        } else if (screenType == Constants.BRIEF_SINGLE_ITEM_SCREEN) {
            ((HoveredTextViewWithSpan) holder.title).setSpanSize(35, 12, 2);
            SpannableString title = new SpannableString("Culpa recusandae eius in atque libero. Eveniet eos eos sed sint et autem.  ");
            holder.title.setText(Constants.setSpan(context, title, (int) Constants.dpFromPx(35, context)
                    , (int) Constants.dpFromPx(12, context), R.mipmap.if_new_36223, -10));
            holder.briefTwoFour54.setSpanSize(20, 20, 1);
            holder.briefTwoFour54.setText(Constants.setSpan(context, "TWOFOUR54", 20, 20));
        } else if (screenType == Constants.INBOX_SCREEN) {
            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.SEND_MESSAGE_ITEM_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
            holder.inboxArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Rect r = Constants.locateView(view);
                    LayoutInflater lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View popup_view = lInflater.inflate(R.layout.popup_layout, null);
                    final PopupWindow popup = new PopupWindow(popup_view, 700, FrameLayout.LayoutParams.WRAP_CONTENT, true);
                    popup.setFocusable(true);
                    popup.setBackgroundDrawable(new ColorDrawable());
                    popup.showAtLocation(view, Gravity.TOP | Gravity.CENTER, r.centerX() - 275, r.bottom + 40);
                    ((TextView) popup_view.findViewById(R.id.first_txt)).setText("all");
                    ((TextView) popup_view.findViewById(R.id.second_txt)).setText("unread");
                    ((TextView) popup_view.findViewById(R.id.third_txt)).setText("none");

                    ((TextView) popup_view.findViewById(R.id.first_txt)).setTextColor(Color.parseColor("#59596A"));
                    ((TextView) popup_view.findViewById(R.id.second_txt)).setTextColor(Color.parseColor("#59596A"));
                    ((TextView) popup_view.findViewById(R.id.third_txt)).setTextColor(Color.parseColor("#59596A"));
                    popup_view.findViewById(R.id.third_txt).setVisibility(View.VISIBLE);
                }
            });
        }/*else if(screenType == Constants.SEND_MESSAGE_ITEM_SCREEN){
            holder.create.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((AppCompatActivity) context)
                            .getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fragment_container, new TermsFragment(Constants.SEND_MESSAGE_ITEM_SCREEN));
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });
        }*/
    }

    private void setCompanies(int pageNum, ViewHolder holder) {
        View v = null;
        for (int i = 0; i < pageNum; i++) {

            final HoveredTextView hoveredTextView = new HoveredTextView(context);
            hoveredTextView.setText((new ArrayList<>(companiesList.keySet())).get(i));
            hoveredTextView.setTextColor(Color.parseColor("#235FAC"));
            hoveredTextView.setId(i);

            holder.companiesCont.addView(hoveredTextView);
            hoveredTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, hoveredTextView.getId() + "", Toast.LENGTH_SHORT).show();
                    //Constants.openLinks(context,(new ArrayList<String>(companiesList.values())).get(view.getId()) );
                }
            });
        }
    }

    private void setPages(int pageNum, ViewHolder holder) {
        View v = null;
        for (int i = 0; i < pageNum; i++) {
            LayoutInflater inflater = LayoutInflater.from(context);
            v = inflater.inflate(R.layout.page_number_list_item, null);
            final TextView pagNum = (TextView) v.findViewById(R.id.page_number);
            pagNum.setId(i);

            if (i == 0)
                pagNum.setText("\u00AB");
            else if (i == pageNum - 1)
                pagNum.setText("\u00BB");
            else
                pagNum.setText(i + "");

            holder.pageNumCon.addView(v);
            pagNum.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, pagNum.getId() + "", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void setInnerRv(RecyclerView rv) {
        rv.setHasFixedSize(true);
        rv.setNestedScrollingEnabled(false);
        // use a linear layout manager
        rv.setLayoutManager(new LinearLayoutManager(context));
    }

    private void setInnerHorizontalRv(RecyclerView rv) {
        rv.setHasFixedSize(true);
        rv.setNestedScrollingEnabled(false);
        // use a linear layout manager
        rv.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
    }

    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position).getViewType();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    @Override
    public void onPagerItemClick(View view, int position) {
        //on freelancer item list clicked
    }

    ;

    @Override
    public void onClick(View view) {
        Constants.openLinks(context, view.getTag().toString());
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        public HoveredTextViewWithSpan briefTwoFour54;
        //These are the general elements in the RecyclerView
        public View v, partnersAdmin, userManagerAdmin, commissionersAdmin, secondParent, exportContainer, titleArrow;
        TextView title, eventTitle, getTouchTitle, listingTitle, preScreenTitle, serviceFeedBackTxt, costaTxt, nathalieTxt, subwayTxt,
                starbucksTxt, foodonclickTxt, talabatTxt, h24Txt, zomatoTxt, addDomain, create, view,
                booking, replaceCard, accessCard, catgTxt, srryTxt, partnerProfileTitle, parContDet, parLNews, parEvents, parBrief, parTweets, exportTxt;
        HorizontalScrollView hsv;
        NonSwipeableViewPager viewPager;
        ListView lv, eventslv;
        MaterialSpinner spinner, categSpinner, skillsSpinner, yearsSpinner, monthsSpinner, ordersSpinner, allSpinner;
        TabLayout mTabLayout;
        RecyclerView mRecyclerView, eventsRv, newslistingRv;
        Button readAllnewsBtn, readAlleventsBtn;
        LinearLayout pageNumCon, companiesCont, topLayout;
        FrameLayout allSpinnerParent , inboxArrow;
        RadioGroup rd;
        ImageView exportArrow;

        //This constructor would switch what to findViewBy according to the type of viewType
        public ViewHolder(View v, int viewType) {
            super(v);
            this.v = v;
            switch (viewType) {
                case 1:
                case 15:
                    title = (TextView) v.findViewById(R.id.screen_title);
                    eventTitle = (TextView) v.findViewById(R.id.event_title1);
                    lv = (ListView) v.findViewById(R.id.inner_lv);
                    mRecyclerView = (RecyclerView) v.findViewById(R.id.inner_rv);
                    eventsRv = (RecyclerView) v.findViewById(R.id.events_rv);
                    viewPager = (NonSwipeableViewPager) v.findViewById(R.id.ln_pager);
                    mTabLayout = (TabLayout) v.findViewById(R.id.pager_header);
                    serviceFeedBackTxt = (HoveredTextViewWithSpan) v.findViewById(R.id.service_feedback_txt);

                    categSpinner = (MaterialSpinner) v.findViewById(R.id.par_category_spinner);
                    skillsSpinner = (MaterialSpinner) v.findViewById(R.id.skillset_spinner);
                    yearsSpinner = (MaterialSpinner) v.findViewById(R.id.year_spinner);
                    monthsSpinner = (MaterialSpinner) v.findViewById(R.id.month_spinner);
                    ordersSpinner = (MaterialSpinner) v.findViewById(R.id.brief_order_spinner);
                    getTouchTitle = (TextView) v.findViewById(R.id.get_in_touch_title);

                    costaTxt = (HoveredTextView) v.findViewById(R.id.costa_txt);
                    nathalieTxt = (HoveredTextView) v.findViewById(R.id.nathalie_txt);
                    subwayTxt = (HoveredTextView) v.findViewById(R.id.subway_txt);
                    starbucksTxt = (HoveredTextView) v.findViewById(R.id.starbucks_txt);
                    foodonclickTxt = (HoveredTextView) v.findViewById(R.id.foodonclick_txt);
                    talabatTxt = (HoveredTextView) v.findViewById(R.id.talabat_txt);
                    h24Txt = (HoveredTextView) v.findViewById(R.id.h24_txt);
                    zomatoTxt = (HoveredTextView) v.findViewById(R.id.zomato_txt);

                    addDomain = (HoveredTextView) v.findViewById(R.id.add_domain);

                    readAllnewsBtn = (AppCompatButton) v.findViewById(R.id.read_allnews_btn);
                    readAlleventsBtn = (AppCompatButton) v.findViewById(R.id.read_allevents_btn);

                    pageNumCon = v.findViewById(R.id.page_num_container);
                    companiesCont = v.findViewById(R.id.companies_individuals_container);

                    newslistingRv = (RecyclerView) v.findViewById(R.id.inner_rv);

                    topLayout = v.findViewById(R.id.top_layout);
                    allSpinnerParent = (FrameLayout) v.findViewById(R.id.all_spinner_parent);
                    catgTxt = (TextView) v.findViewById(R.id.catg_txt);
                    preScreenTitle = (TextView) v.findViewById(R.id.pre_screen_title);
                    listingTitle = (TextView) v.findViewById(R.id.listing_title);
                    create = (HoveredTextView) v.findViewById(R.id.create);
                    view = (HoveredTextView) v.findViewById(R.id.view);
                    srryTxt = (TextView) v.findViewById(R.id.srry_txt);
                    hsv = (HorizontalScrollView) v.findViewById(R.id.hsv);
                    secondParent = v.findViewById(R.id.scond_container);
                    rd = (RadioGroup) v.findViewById(R.id.listing_rd);
                    exportContainer = v.findViewById(R.id.export_container);
                    exportTxt = (HoveredTextView) v.findViewById(R.id.export_txt);
                    exportArrow = (ImageView) v.findViewById(R.id.export_arrow);
                    titleArrow = v.findViewById(R.id.title_arrow);
                    allSpinner = (MaterialSpinner) v.findViewById(R.id.all_spinner);

                    booking = (HoveredTextView) v.findViewById(R.id.booking_form);
                    replaceCard = (HoveredTextView) v.findViewById(R.id.card_repalce);
                    accessCard = (HoveredTextView) v.findViewById(R.id.access_card);

                    partnerProfileTitle = (HoveredTextViewWithSpan) v.findViewById(R.id.partner_profile_name);
                    parContDet = (HoveredTextView) v.findViewById(R.id.par_cont_det);
                    parTweets = (HoveredTextView) v.findViewById(R.id.par_tweet);
                    parBrief = (HoveredTextView) v.findViewById(R.id.par_bref);
                    parEvents = (HoveredTextView) v.findViewById(R.id.par_events);
                    parLNews = (HoveredTextView) v.findViewById(R.id.par_lnews);

                    title.setTypeface(Constants.setTitleTypeFace(context));

                    briefTwoFour54 = (HoveredTextViewWithSpan) v.findViewById(R.id.brief_single_item_twofour);

                    inboxArrow = v.findViewById(R.id.inbox_arrow);

                    break;
                case 11:
                    title = (TextView) v.findViewById(R.id.screen_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 12:
                    title = (TextView) v.findViewById(R.id.screen_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 13:
                    lv = (ListView) v.findViewById(R.id.inner_lv);
                    title = (TextView) v.findViewById(R.id.screen_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 14:
                    title = (TextView) v.findViewById(R.id.screen_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 16:
                    title = (TextView) v.findViewById(R.id.mesamix_admin_title);
                    commissionersAdmin = v.findViewById(R.id.commissioners_admin);
                    partnersAdmin = v.findViewById(R.id.partner_admin);
                    userManagerAdmin = v.findViewById(R.id.user_manager_admin);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 17:
                    title = (TextView) v.findViewById(R.id.add_news_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 18:
                    title = (TextView) v.findViewById(R.id.classifieds_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 19:
                    title = (TextView) v.findViewById(R.id.twofour54_serv_title);
                    title.setTypeface(Constants.setTitleTypeFace(context));
                    break;
                case 20:
                    spinner = (MaterialSpinner) v.findViewById(R.id.feedback_spinner);
                    break;
            }
        }
    }

    class WizardPagerAdapter extends PagerAdapter {

        private final int viewType;
        View v;

        private String[] newsTabTitles = new String[]{"get in\n touch", "service\n request", "feedback"};

        public WizardPagerAdapter(View view, int viewType) {
            v = view;
            this.viewType = viewType;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return newsTabTitles[position];
        }

        public Object instantiateItem(ViewGroup collection, int position) {

            int resId = 0;
            switch (position) {
                case 0:
                    resId = R.id.page_one;
                    break;
                case 1:
                    resId = R.id.page_two;
                    break;
                case 2:
                    resId = R.id.page_three;
                    break;
            }

            return v.findViewById(resId);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }
    }
}