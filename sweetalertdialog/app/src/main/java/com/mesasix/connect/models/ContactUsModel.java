package com.mesasix.connect.models;

/**
 * Created by Esraa.Nayel on 4/12/2018.
 */

public class ContactUsModel {


    private String name;
    private String desc;
    private String mobile;
    private String mail;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
