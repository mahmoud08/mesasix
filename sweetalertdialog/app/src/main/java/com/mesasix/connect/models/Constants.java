package com.mesasix.connect.models;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.mesasix.connect.R;
import com.mesasix.connect.views.CenteredImageSpan;

/**
 * Created by Esraa.Nayel on 4/22/2018.
 */

public class Constants {

    public static final int NEWS_EVENTS_SCREEN = 2;
    public static final int BRIEF_SCREEN = 3;
    public static final int PARTENER_SCREEN = 4;
    public static final int FREELANCER_SCREEN = 5;
    public static final int SERVICES_SCREEN = 6;
    public static final int JOBS_SCREEN =7;
    public static final int CONTACT_US_SCREEN = 9;

    public static final int ABOUT_SCREEN = 111;
    public static final int TERMS_SCREEN = 112;
    public static final int FEEDBACK_SCREEN = 113;
    public static final int ACCOUNT_SETTINGS = 114;
    public static final int PASS_SCREEN = 115;
    public static final int USER_MANAG_SCREEN = 116;
    public static final int SPAM_MANAG_SCREEN = 117;
    public static final int COMMISIONERS_MANAG_SCREEN = 150;
    public static final int CREATE_NEWS_SCREEN = 118;
    public static final int NEWS_LISTING_SCREEN = 1181;
    public static final int CREATE_EVENTS_SCREEN = 119;
    public static final int EVENTS_LISTING_SCREEN= 1191;
    public static final int CREATE_BRIEF_SCREEN = 120;
    public static final int BRIEF_LISTING_SCREEN= 1201;
    public static final int CREATE_JOB_SCREEN = 121;
    public static final int JOB_LISTING_SCREEN = 1211;
    public static final int CREATE_PHOTO_SCREEN = 122;
    public static final int PHOTO_LISTING_SCREEN = 1221;
    public static final int CREATE_VIDEO_SCREEN = 123;
    public static final int VIDEO_SCREEN = 1231;
    public static final int CREATE_TESTIMONIAL_SCREEN = 124;
    public static final int TESTIMONIAL_LISTING_SCREEN = 1241;
    public static final int CREATE_CLASSIFIED_SCREEN = 125;
    public static final int CLASSIFIED_LISTING_SCREEN = 1251;
    public static final int CREATE_BLOG_SCREEN = 126;
    public static final int BLOG_LISTING_SCREEN = 1261;
    public static final int CREATE_SERVICE_SCREEN = 127;
    public static final int SERVICE_LISTING_SCREEN = 1271;
    public static final int CREATE_COMMISSIONERS_SCREEN = 128;
    public static final int COMMISSIONERS_LISTING_SCREEN = 1281;
    public static final int CREATE_PARTNER_SCREEN = 129;
    public static final int PARTNER_LISTING_SCREEN = 1291;

    public static final int USER_MANAGEMENT_LISTING_SCREEN = 130;

    public static final int ORDERS_SCREEN = 201;
    public static final int CLASSIFIEDS_SCREEN = 202;

    public static final int CREATE_FREELANCER_SCREEN = 131;
    public static final int FREELANCER_LISTING_SCREEN = 1311;
    public static final int CREATE_PARTNER_USER_SCREEN = 132;


    public static final int ALL_NEWS_SCREEN = 10;
    public static final int ALL_EVENTS_SCREEN = 11;

    public static final int SERVICES_COMPANIES_SCREEN = 61;
    public static final int SERVICES_TRAVEL_SCREEN = 62;
    public static final int SERVICES_INTERNSHIP_SCREEN = 63;
    public static final int SERVICES_WELLNES_SCREEN = 64;
    public static final int SERVICES_SEMINAR_SCREEN = 65;
    public static final int SERVICES_INDIVIDUALS_SCREEN = 66;
    public static final int SERVICES_OFFICE_SCREEN = 67;

    public static final int PARTENER_PROFILE_SCREEN = 1111;
    public static final int FREELANCER_PROFILE_SCREEN = 1112;
    public static final int NEWS_SINGLE_ITEM_SCREEN = 1113;
    public static final int EVENTS_SINGLE_ITEM_SCREEN = 1114;
    public static final int BRIEF_SINGLE_ITEM_SCREEN = 1115;

    public static final int INBOX_SCREEN = 300;
    public static final int SEND_MESSAGE_ITEM_SCREEN = 301;

    public static Rect locateView(View v) {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe) {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static void openLinks(Context context ,String link){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(link));
        context.startActivity(browserIntent);
    }

    public static SpannableString setSpan(Context context, String text, int txtWidth, int txtHeight) {
        SpannableString ssBuilder = new SpannableString(text + " ");
        Drawable doubleArrowIcon = context.getResources().getDrawable(R.drawable.arrows_unselected);
        int width = (int) dpFromPx(txtWidth, context);
        int height = (int) dpFromPx(txtHeight, context);
        doubleArrowIcon.setBounds(0, 0, width, height);
        CenteredImageSpan doubleArrowSpan = new CenteredImageSpan(doubleArrowIcon, ImageSpan.ALIGN_BASELINE);

        ssBuilder.setSpan(
                doubleArrowSpan, // Span to add
                ssBuilder.length() - 1, // Start of the span (inclusive)
                ssBuilder.length(), // End of the span (exclusive)
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);// Do not extend the span when text add later
        return ssBuilder;
    }

    public static SpannableString setSpan(Context context,
                                          SpannableString ssBuilder, int width, int height, int drawable, int ypos) {
        //SpannableString ssBuilder = new SpannableString(text + "  ");
        Drawable doubleArrowIcon = context.getResources().getDrawable(drawable);
        //int width = (int) dpFromPx(txtWidth, context);
        //int height = (int) dpFromPx(txtHeight, context);
        doubleArrowIcon.setBounds(0, 0, width, height);
        CenteredImageSpan doubleArrowSpan = new CenteredImageSpan(doubleArrowIcon, ImageSpan.ALIGN_BASELINE);
        //doubleArrowSpan.setyPos(ypos);
        if (ypos != 0)
            doubleArrowSpan.setyPos(ypos);
        ssBuilder.setSpan(
                doubleArrowSpan, // Span to add
                ssBuilder.length() - 1, // Start of the span (inclusive)
                ssBuilder.length(), // End of the span (exclusive)
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);// Do not extend the span when text add later
        return ssBuilder;
    }

    public static Typeface setTitleTypeFace(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/open_sans_semi_bld.ttf");
    }

    public static float dpFromPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

}
