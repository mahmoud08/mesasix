package com.mesasix.connect.views;

import android.content.Context;
import android.text.Layout;
import android.util.AttributeSet;
import android.widget.TextView;

public class BaselineLastLineTextView extends android.support.v7.widget.AppCompatTextView {

    public BaselineLastLineTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public int getBaseline() {
        Layout layout = getLayout();
        if (layout == null) {
            return super.getBaseline();
        }
        int baselineOffset = super.getBaseline() - layout.getLineBaseline(0);
        return baselineOffset + layout.getLineBaseline(layout.getLineCount()-1);
    }
}