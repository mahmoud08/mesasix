package com.chabbal.slidingdotsplash;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.ArrayRes;
import android.support.annotation.NonNull;
import android.support.annotation.Size;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import java.util.List;

/**
 * Created by Johny on 28/01/2017.
 */

public class SlidingSplashView extends FrameLayout {

    public ViewPager mViewPager;
    ViewPager.OnPageChangeListener mOnPageChangeListener;
    int[] imgs = new int[]{R.drawable.test, R.drawable.test, R.drawable.test, R.drawable.test};
    private ImageViewPagerAdapter mViewPagerAdapter;
    private OnSetImageListener mOnSetImageListener;

    public SlidingSplashView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SlidingSplashView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
    }

    public void initViewPager(Context context, List<ItemModel> models, int type, OnItemClickListener onItemClickListener) {
        LayoutInflater.from(context).inflate(type == 1
                ? R.layout.sliding_splash_view : R.layout.sliding_splash_view2, this);
        mViewPager = (ViewPager) findViewById(R.id.pager_splash);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabDots);
        tabLayout.setupWithViewPager(mViewPager, true);

        if (type == 1) {
            mViewPagerAdapter = new ImageViewPagerAdapter(context, mOnSetImageListener, type);
            mViewPagerAdapter.setImageResources(models);
            mViewPager.setAdapter(mViewPagerAdapter);
            mViewPagerAdapter.setOnPagerItemClick(onItemClickListener);
        } else {
            /*ViewGroup.LayoutParams params = mViewPager.getLayoutParams();
            params.height=600;
            mViewPager.setLayoutParams(params);*/
            ImageViewPagerAdapter2 mViewPagerAdapter = new ImageViewPagerAdapter2(context, mOnSetImageListener, type);
            mViewPagerAdapter.setImageResources(models);
            mViewPager.setAdapter(mViewPagerAdapter);
            mViewPagerAdapter.setOnPagerItemClick(onItemClickListener);
        }
    }

    public void setImageResources(@SuppressLint("SupportAnnotationUsage") @NonNull @ArrayRes @Size(min = 2) List<ItemModel> imageResources) {
        mViewPagerAdapter.setImageResources(imageResources);
    }

    public void setOnShowImageListener(OnSetImageListener onShowImageListener) {
        mOnSetImageListener = onShowImageListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mViewPagerAdapter.setOnPagerItemClick(onItemClickListener);
    }

    public void addOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        mViewPager.addOnPageChangeListener(mOnPageChangeListener = onPageChangeListener);
    }

    public void removeOnPageChangeListener() {
        mViewPager.removeOnPageChangeListener(mOnPageChangeListener);
    }

}
